-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 07 Bulan Mei 2024 pada 18.41
-- Versi server: 10.4.27-MariaDB
-- Versi PHP: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_stok_barang`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `remember_token` varchar(200) DEFAULT NULL,
  `link_foto` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id`, `nama`, `email`, `password`, `remember_token`, `link_foto`, `created_at`, `updated_at`) VALUES
(3, 'Bambang Yudi Nugraha', 'admin@admin.com', '$2y$10$peFqFu/5j5UUmIptbKVdlO5rUpwlucL9TIzfWdGdmsodr3sDl.oMO', 'NvnXDaH2yvRD7PKgs8qyjWxKOTqfrvudX5FFEvUq6oWzTls5AaUPcKzoQCSc', '/storage/administrator/RMigq7er3x4WneX.jpeg', '2024-05-07 16:34:57', '2021-08-20 18:51:22'),
(4, 'Ashri Ajizah Nurahmah', 'tpjcenter@gmail.com', '$2y$10$MDqwWH.6qGA.KBfwrFvLj.1QrlSFF1QKud9sxJaLFmqrU5lsr02fe', 'fsbIhDA7rf9h95iHwGL2ChiX2uH95i7i7GTv68yD51bMORzRVUvBZq0tQtDq', '/storage/iklan/aUQsBjn1zbCuA4F.png', '2023-03-06 01:12:50', NULL),
(5, 'Ashri Ajizah Nurahmah', 'ashriajizahn@gmail.com', '$2y$10$VjYXZMkrw3lLFfww7mx0NO4fDCYVacCdExGNQ0nuvs9XLxVmwRhOW', 'Mh1uWA0awmtLbI1WPRHhrS3LMMwMRicYN8eMOovSCil9XMbmQlNiRCQyrOga', '/storage/administrator/y6mZIn3HJaNNdsh.png', '2023-06-07 02:19:13', '2023-03-06 01:13:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `log_error`
--

CREATE TABLE `log_error` (
  `id` int(11) NOT NULL,
  `log_error` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `log_error`
--

INSERT INTO `log_error` (`id`, `log_error`, `created_at`) VALUES
(1, '{\"status\":false,\"message\":\"Error\",\"data\":\"json_decode() expects parameter 1 to be string, array given\",\"post\":[{\"kuisoner_id\":1,\"jawaban\":\"Ya\"},{\"kuisoner_id\":2,\"jawaban\":\"Tidak\"},{\"kuisoner_id\":3,\"jawaban\":\"Tidak\"},{\"kuisoner_id\":4,\"jawaban\":\"Ragu-Ragu\"}]}', '2023-01-21 19:19:00'),
(2, '{\"status\":false,\"message\":\"Error\",\"data\":\"count(): Parameter must be an array or an object that implements Countable\",\"post\":\"[{\\\"kuisoner_id\\\":\\\"0\\\",\\\"jawaban\\\":\\\"2\\\"}, {\\\"kuisoner_id\\\":\\\"1\\\",\\\"jawaban\\\":\\\"3\\\"}, {\\\"kuisoner_id\\\":\\\"2\\\",\\\"jawaban\\\":\\\"2\\\"}, {\\\"kuisoner_id\\\":\\\"3\\\",\\\"jawaban\\\":\\\"1\\\"}, {\\\"kuisoner_id\\\":\\\"4\\\",\\\"jawaban\\\":\\\"2\\\"}, {\\\"kuisoner_id\\\":\\\"5\\\",\\\"jawaban\\\":\\\"1\\\"}, {\\\"kuisoner_id\\\":\\\"6\\\",\\\"jawaban\\\":\\\"1\\\"}, {\\\"kuisoner_id\\\":\\\"7\\\",\\\"jawaban\\\":\\\"1\\\"}, {\\\"kuisoner_id\\\":\\\"8\\\",\\\"jawaban\\\":\\\"0\\\"}]\"}', '2023-01-22 06:09:00'),
(3, '{\"status\":false,\"message\":\"Error\",\"data\":\"count(): Parameter must be an array or an object that implements Countable\",\"post\":\"[{\\\"kuisoner_id\\\":\\\"0\\\",\\\"jawaban\\\":\\\"2\\\"}, {\\\"kuisoner_id\\\":\\\"1\\\",\\\"jawaban\\\":\\\"3\\\"}, {\\\"kuisoner_id\\\":\\\"2\\\",\\\"jawaban\\\":\\\"2\\\"}, {\\\"kuisoner_id\\\":\\\"3\\\",\\\"jawaban\\\":\\\"1\\\"}, {\\\"kuisoner_id\\\":\\\"4\\\",\\\"jawaban\\\":\\\"2\\\"}, {\\\"kuisoner_id\\\":\\\"5\\\",\\\"jawaban\\\":\\\"1\\\"}, {\\\"kuisoner_id\\\":\\\"6\\\",\\\"jawaban\\\":\\\"1\\\"}, {\\\"kuisoner_id\\\":\\\"7\\\",\\\"jawaban\\\":\\\"1\\\"}, {\\\"kuisoner_id\\\":\\\"8\\\",\\\"jawaban\\\":\\\"0\\\"}]\"}', '2023-01-22 06:10:00'),
(4, '{\"status\":false,\"message\":\"Error\",\"data\":\"count(): Parameter must be an array or an object that implements Countable\",\"post\":\"[{\\\"kuisoner_id\\\":\\\"0\\\",\\\"jawaban\\\":\\\"2\\\"}, {\\\"kuisoner_id\\\":\\\"1\\\",\\\"jawaban\\\":\\\"3\\\"}, {\\\"kuisoner_id\\\":\\\"2\\\",\\\"jawaban\\\":\\\"2\\\"}, {\\\"kuisoner_id\\\":\\\"3\\\",\\\"jawaban\\\":\\\"1\\\"}, {\\\"kuisoner_id\\\":\\\"4\\\",\\\"jawaban\\\":\\\"2\\\"}, {\\\"kuisoner_id\\\":\\\"5\\\",\\\"jawaban\\\":\\\"1\\\"}, {\\\"kuisoner_id\\\":\\\"6\\\",\\\"jawaban\\\":\\\"1\\\"}, {\\\"kuisoner_id\\\":\\\"7\\\",\\\"jawaban\\\":\\\"1\\\"}, {\\\"kuisoner_id\\\":\\\"8\\\",\\\"jawaban\\\":\\\"0\\\"}]\"}', '2023-01-22 06:10:00'),
(5, '{\"status\":false,\"message\":\"Error\",\"data\":\"count(): Parameter must be an array or an object that implements Countable\",\"post\":\"[{\\\"kuisoner_id\\\":\\\"0\\\",\\\"jawaban\\\":\\\"2\\\"}, {\\\"kuisoner_id\\\":\\\"1\\\",\\\"jawaban\\\":\\\"3\\\"}, {\\\"kuisoner_id\\\":\\\"2\\\",\\\"jawaban\\\":\\\"2\\\"}, {\\\"kuisoner_id\\\":\\\"3\\\",\\\"jawaban\\\":\\\"1\\\"}, {\\\"kuisoner_id\\\":\\\"4\\\",\\\"jawaban\\\":\\\"2\\\"}, {\\\"kuisoner_id\\\":\\\"5\\\",\\\"jawaban\\\":\\\"1\\\"}, {\\\"kuisoner_id\\\":\\\"6\\\",\\\"jawaban\\\":\\\"1\\\"}, {\\\"kuisoner_id\\\":\\\"7\\\",\\\"jawaban\\\":\\\"1\\\"}, {\\\"kuisoner_id\\\":\\\"8\\\",\\\"jawaban\\\":\\\"0\\\"}]\"}', '2023-01-22 06:10:00'),
(6, '{\"status\":false,\"message\":\"Error\",\"data\":\"count(): Parameter must be an array or an object that implements Countable\",\"post\":\"[{\\\"kuisoner_id\\\":\\\"0\\\",\\\"jawaban\\\":\\\"2\\\"}, {\\\"kuisoner_id\\\":\\\"1\\\",\\\"jawaban\\\":\\\"3\\\"}, {\\\"kuisoner_id\\\":\\\"2\\\",\\\"jawaban\\\":\\\"2\\\"}, {\\\"kuisoner_id\\\":\\\"3\\\",\\\"jawaban\\\":\\\"1\\\"}, {\\\"kuisoner_id\\\":\\\"4\\\",\\\"jawaban\\\":\\\"2\\\"}, {\\\"kuisoner_id\\\":\\\"5\\\",\\\"jawaban\\\":\\\"1\\\"}, {\\\"kuisoner_id\\\":\\\"6\\\",\\\"jawaban\\\":\\\"1\\\"}, {\\\"kuisoner_id\\\":\\\"7\\\",\\\"jawaban\\\":\\\"1\\\"}, {\\\"kuisoner_id\\\":\\\"8\\\",\\\"jawaban\\\":\\\"0\\\"}]\"}', '2023-01-22 06:10:00'),
(7, '{\"status\":false,\"message\":\"Error\",\"data\":\"SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'atribut_apk\' cannot be null (SQL: insert into `m_canvasser` (`uuid_user`, `nik`, `kode_laporan`, `type_laporan`, `status_laporan`, `status_dpt`, `nama_warga`, `jenis_kelamin`, `jumlah_hak_pilih`, `atribut_apk`, `link_gambar`, `created_at`) values (TIM-SWT5-0007, 3204171710420001, REG\\/2023\\/02\\/03\\/0024, 1, 0, 0, BRUCE ROYDEN OGLE, MALE, 0, ?, \\/storage\\/gambar\\/U5nUFOElbWgMhwb.jpg, 2023-02-03 21:27:41))\"}', '2023-02-03 14:27:00'),
(8, '{\"status\":false,\"message\":\"Error\",\"data\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'nomor_tps\' in \'field list\' (SQL: insert into `m_canvasser` (`uuid_user`, `nik`, `kode_laporan`, `type_laporan`, `status_laporan`, `no_tps`, `nomor_tps`, `status_dpt`, `nama_warga`, `jenis_kelamin`, `jumlah_hak_pilih`, `atribut_apk`, `link_gambar`, `created_at`) values (TIM-DECC-0013, 3203012503770011, REG\\/2023\\/03\\/04\\/0011, 1, 0, 0, 081221538869, 0, GUOHUI CHEN, LAKI-LAKI, 0, ?, \\/storage\\/gambar\\/qujapk0cyAKt3Hz.jpg, 2023-03-04 14:45:59))\"}', '2023-03-04 07:45:00'),
(9, '{\"status\":false,\"message\":\"Error\",\"data\":\"Undefined variable: berita\"}', '2023-03-04 07:50:00'),
(10, '{\"status\":false,\"message\":\"Error\",\"data\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'emosional\' in \'field list\' (SQL: insert into `m_canvasser_lokasi` (`canvasser_id`, `kota`, `kecamatan`, `desa`, `rt`, `rw`, `alamat`, `latitude`, `longitude`, `emosional`, `created_at`) values (1401, Kab. Bandung, Paseh, Mekarpawitan, 3, 2, Jl. Kadatuan No.51, Mekarpawitan, Kec. Paseh, Kabupaten Bandung, Jawa Barat 40383, Indonesia, -7.0498938, 107.7797018, 0.0, 2023-06-02 10:28:21))\",\"post\":\"[{\\\"kuisoner_id\\\":\\\"6\\\",\\\"jawaban\\\":\\\"Asep\\\"}, {\\\"kuisoner_id\\\":\\\"7\\\",\\\"jawaban\\\":\\\"ya\\\"}]\"}', '2023-06-02 03:28:00'),
(11, '{\"status\":false,\"message\":\"Error\",\"data\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'emosional\' in \'field list\' (SQL: insert into `m_canvasser_lokasi` (`canvasser_id`, `kota`, `kecamatan`, `desa`, `rt`, `rw`, `alamat`, `latitude`, `longitude`, `emosional`, `created_at`) values (1402, Kab. Bandung, Paseh, Mekarpawitan, 3, 2, Jl. Kadatuan No.51, Mekarpawitan, Kec. Paseh, Kabupaten Bandung, Jawa Barat 40383, Indonesia, -7.0498886, 107.7797246, 0.0, 2023-06-02 10:31:32))\",\"post\":\"[{\\\"kuisoner_id\\\":\\\"6\\\",\\\"jawaban\\\":\\\"Asep\\\"}, {\\\"kuisoner_id\\\":\\\"7\\\",\\\"jawaban\\\":\\\"ya\\\"}]\"}', '2023-06-02 03:31:00'),
(12, '{\"status\":false,\"message\":\"Error\",\"data\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'emosional\' in \'field list\' (SQL: insert into `m_canvasser_lokasi` (`canvasser_id`, `kota`, `kecamatan`, `desa`, `rt`, `rw`, `alamat`, `latitude`, `longitude`, `emosional`, `created_at`) values (1403, Kab. Bandung, Paseh, Mekarpawitan, 3, 2, Jl. Kadatuan No.51, Mekarpawitan, Kec. Paseh, Kabupaten Bandung, Jawa Barat 40383, Indonesia, -7.0499102, 107.7797446, 0.0, 2023-06-02 10:39:52))\",\"post\":\"[{\\\"kuisoner_id\\\":\\\"6\\\",\\\"jawaban\\\":\\\"Ari\\\"}, {\\\"kuisoner_id\\\":\\\"7\\\",\\\"jawaban\\\":\\\"ya\\\"}]\"}', '2023-06-02 03:39:00'),
(13, '{\"status\":false,\"message\":\"Error\",\"data\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'emosional\' in \'field list\' (SQL: insert into `m_canvasser_lokasi` (`canvasser_id`, `kota`, `kecamatan`, `desa`, `rt`, `rw`, `alamat`, `latitude`, `longitude`, `emosional`, `created_at`) values (1404, Kab. Bandung, Paseh, Mekarpawitan, 3, 2, Jl. Kadatuan No.51, Mekarpawitan, Kec. Paseh, Kabupaten Bandung, Jawa Barat 40383, Indonesia, -7.0499316, 107.7797234, 0.0, 2023-06-02 10:44:06))\",\"post\":\"[{\\\"kuisoner_id\\\":\\\"6\\\",\\\"jawaban\\\":\\\"Ari\\\"}, {\\\"kuisoner_id\\\":\\\"7\\\",\\\"jawaban\\\":\\\"ya\\\"}]\"}', '2023-06-02 03:44:00'),
(14, '{\"status\":false,\"message\":\"Error\",\"data\":\"SQLSTATE[42S22]: Column not found: 1054 Unknown column \'emosional\' in \'field list\' (SQL: insert into `m_canvasser_lokasi` (`canvasser_id`, `kota`, `kecamatan`, `desa`, `rt`, `rw`, `alamat`, `latitude`, `longitude`, `emosional`, `created_at`) values (1405, Kab. Bandung, Paseh, Cipaku, 03, 04, WQXG+6P4, Cipaku, Kec. Paseh, Kabupaten Bandung, Jawa Barat 40383, Indonesia, -7.051965, 107.7766044, 0.02221816, 2023-06-02 12:26:04))\",\"post\":\"[{\\\"kuisoner_id\\\":\\\"6\\\",\\\"jawaban\\\":\\\"Bambang\\\"}, {\\\"kuisoner_id\\\":\\\"7\\\",\\\"jawaban\\\":\\\"ya\\\"}]\"}', '2023-06-02 05:26:00'),
(15, '{\"status\":false,\"message\":\"Error\",\"data\":\"json_decode() expects parameter 1 to be string, array given\",\"post\":[{\"kuisoner_id\":1,\"jawaban\":\"Ya\"},{\"kuisoner_id\":2,\"jawaban\":\"Tidak\"},{\"kuisoner_id\":3,\"jawaban\":\"Tidak\"},{\"kuisoner_id\":4,\"jawaban\":\"Ragu-Ragu\"}]}', '2023-06-11 11:54:00'),
(16, '{\"status\":false,\"message\":\"Error\",\"data\":\"A non-numeric value encountered\"}', '2023-06-11 12:00:00'),
(17, '{\"status\":false,\"message\":\"Error\",\"data\":\"A non-numeric value encountered\"}', '2023-06-11 12:01:00'),
(18, '{\"status\":false,\"message\":\"Error\",\"data\":\"A non-numeric value encountered\"}', '2023-06-11 12:04:00'),
(19, '{\"status\":false,\"message\":\"Error\",\"data\":\"A non-numeric value encountered\"}', '2023-06-11 12:05:00'),
(20, '{\"status\":false,\"message\":\"Error\",\"data\":\"A non-numeric value encountered\"}', '2023-06-11 12:06:00'),
(21, '{\"status\":false,\"message\":\"Error\",\"data\":\"A non-numeric value encountered\"}', '2023-06-11 12:08:00'),
(22, '{\"status\":false,\"message\":\"Error\",\"data\":\"Undefined property: stdClass::$nomor_hp\"}', '2023-06-11 12:12:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_harga`
--

CREATE TABLE `m_harga` (
  `id` int(11) NOT NULL,
  `kode_produk` varchar(50) NOT NULL,
  `harga_produk` float NOT NULL,
  `satua_produk` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `m_harga`
--

INSERT INTO `m_harga` (`id`, `kode_produk`, `harga_produk`, `satua_produk`, `created_at`, `updated_at`) VALUES
(1, 'PRO-37822', 2000, 'Konsumsi', '2024-05-07 13:36:51', '2024-05-07 13:36:51'),
(2, 'PRO-41503', 1000, 'Pembersih', '2024-05-07 13:36:58', '2024-05-07 13:36:58');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_log_harga`
--

CREATE TABLE `m_log_harga` (
  `id` int(11) NOT NULL,
  `kode_produk` varchar(50) NOT NULL,
  `harga_produk` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `m_log_harga`
--

INSERT INTO `m_log_harga` (`id`, `kode_produk`, `harga_produk`, `created_at`) VALUES
(1, 'PRO-38013', 2000, '2023-07-24 03:49:57'),
(2, 'PRO-38013', 3000, '2023-07-24 04:04:35'),
(3, 'PRO-38013', 3500, '2023-07-24 04:31:06'),
(4, 'PRO-38013', 3200, '2023-07-24 04:51:41'),
(5, 'PRO-38013', 4000, '2023-07-24 04:51:59'),
(6, 'PRO-38608', 8000, '2023-07-24 04:58:28'),
(7, 'PRO-70111', 30000, '2024-05-07 08:47:22'),
(8, 'PRO-56135', 0, '2024-05-07 11:55:07'),
(9, 'PRO-56135', 5000, '2024-05-07 11:55:16'),
(10, 'PRO-37136', 50000, '2024-05-07 12:59:44'),
(11, 'PRO-37822', 2000, '2024-05-07 13:24:38'),
(12, 'PRO-41503', 1000, '2024-05-07 13:25:04'),
(13, 'PRO-37822', 2000, '2024-05-07 13:35:48'),
(14, 'PRO-37822', 2000, '2024-05-07 13:36:51'),
(15, 'PRO-41503', 1000, '2024-05-07 13:36:58');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_produk`
--

CREATE TABLE `m_produk` (
  `id` int(11) NOT NULL,
  `kode_produk` varchar(50) NOT NULL,
  `nama_produk` varchar(100) NOT NULL,
  `link_gambar` varchar(150) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `m_produk`
--

INSERT INTO `m_produk` (`id`, `kode_produk`, `nama_produk`, `link_gambar`, `created_at`, `updated_at`) VALUES
(1, 'PRO-37822', 'Kopi', '/storage/foto/qOYcm8L9Panx68G.jpeg', '2024-05-07 13:23:40', '0000-00-00 00:00:00'),
(2, 'PRO-41503', 'Pasta Gigi', '/storage/foto/ukr5pjZjx37ivy5.jpeg', '2024-05-07 13:24:51', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_stok`
--

CREATE TABLE `m_stok` (
  `id` int(11) NOT NULL,
  `kode_produk` varchar(50) NOT NULL,
  `stok_barang` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `m_stok`
--

INSERT INTO `m_stok` (`id`, `kode_produk`, `stok_barang`, `created_at`, `updated_at`) VALUES
(1, 'PRO-37822', 5, '2024-05-07 13:25:42', NULL),
(2, 'PRO-41503', 5, '2024-05-07 15:15:00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_transaksi_barang`
--

CREATE TABLE `m_transaksi_barang` (
  `id` int(11) NOT NULL,
  `kode_transaksi` varchar(50) NOT NULL,
  `tipe` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `m_transaksi_barang`
--

INSERT INTO `m_transaksi_barang` (`id`, `kode_transaksi`, `tipe`, `created_at`, `updated_at`) VALUES
(1, 'INV-IN-VLBV-0000', 1, '2024-05-07 13:25:17', NULL),
(2, 'INV-IN-753Z-0001', 1, '2024-05-07 13:25:29', NULL),
(3, 'INV-OUT-ALX1-0002', 0, '2024-05-07 13:25:42', NULL),
(4, 'INV-OUT-LYLN-0003', 0, '2024-05-07 13:41:01', '2024-05-07 13:41:01'),
(5, 'INV-OUT-G6AU-0004', 0, '2024-05-07 15:07:25', NULL),
(6, 'INV-OUT-4NVG-0005', 0, '2024-05-07 15:15:00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_transaksi_barang`
--

CREATE TABLE `t_transaksi_barang` (
  `id` int(11) NOT NULL,
  `kode_transaksi` varchar(50) NOT NULL,
  `kode_barang` varchar(50) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data untuk tabel `t_transaksi_barang`
--

INSERT INTO `t_transaksi_barang` (`id`, `kode_transaksi`, `kode_barang`, `jumlah`, `created_at`) VALUES
(1, 'INV-IN-VLBV-0000', 'PRO-37822', 10, '2024-05-07 13:25:17'),
(2, 'INV-IN-753Z-0001', 'PRO-41503', 20, '2024-05-07 13:25:29'),
(3, 'INV-OUT-ALX1-0002', 'PRO-37822', 5, '2024-05-07 13:25:42'),
(5, 'INV-OUT-LYLN-0003', 'PRO-41503', 10, '2024-05-07 13:41:01'),
(6, 'INV-OUT-G6AU-0004', 'PRO-41503', 2, '2024-05-07 15:07:25'),
(7, 'INV-OUT-4NVG-0005', 'PRO-41503', 3, '2024-05-07 15:15:00');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `log_error`
--
ALTER TABLE `log_error`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `m_harga`
--
ALTER TABLE `m_harga`
  ADD PRIMARY KEY (`id`),
  ADD KEY `m_harga_ibfk_1` (`kode_produk`);

--
-- Indeks untuk tabel `m_log_harga`
--
ALTER TABLE `m_log_harga`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `m_produk`
--
ALTER TABLE `m_produk`
  ADD PRIMARY KEY (`kode_produk`),
  ADD UNIQUE KEY `id` (`id`) USING BTREE;

--
-- Indeks untuk tabel `m_stok`
--
ALTER TABLE `m_stok`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kode_produk` (`kode_produk`);

--
-- Indeks untuk tabel `m_transaksi_barang`
--
ALTER TABLE `m_transaksi_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `t_transaksi_barang`
--
ALTER TABLE `t_transaksi_barang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kode_barang` (`kode_barang`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `log_error`
--
ALTER TABLE `log_error`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `m_harga`
--
ALTER TABLE `m_harga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `m_log_harga`
--
ALTER TABLE `m_log_harga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `m_produk`
--
ALTER TABLE `m_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `m_stok`
--
ALTER TABLE `m_stok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `m_transaksi_barang`
--
ALTER TABLE `m_transaksi_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `t_transaksi_barang`
--
ALTER TABLE `t_transaksi_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `m_harga`
--
ALTER TABLE `m_harga`
  ADD CONSTRAINT `m_harga_ibfk_1` FOREIGN KEY (`kode_produk`) REFERENCES `m_produk` (`kode_produk`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `m_stok`
--
ALTER TABLE `m_stok`
  ADD CONSTRAINT `m_stok_ibfk_1` FOREIGN KEY (`kode_produk`) REFERENCES `m_produk` (`kode_produk`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `t_transaksi_barang`
--
ALTER TABLE `t_transaksi_barang`
  ADD CONSTRAINT `t_transaksi_barang_ibfk_1` FOREIGN KEY (`kode_barang`) REFERENCES `m_produk` (`kode_produk`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
