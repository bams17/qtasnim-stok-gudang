<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Mail;


class RegistrasiMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function kirimemailregister($nama,$email,$code,$barcode, $type=0)
    {

    if ($type==0) {
        return Mail::to($email)->send(
            $this->from('noreply@harveo.id')
                  ->with(
                    [
                        'nama' => $nama,
                        'email' => $email,
                        'kode_registrasi'=>$code,
                        'barcode' => $barcode
                    ])
                    ->view('pages.admin.layoutemail.registrasi-email')
                    );
    }else{
        return Mail::to($email)->send(
            $this->from('noreply@harveo.id')
                  ->with(
                    [
                        'nama' => $nama,
                        'email' => $email,
                        'kode_registrasi'=>$code,
                        'barcode' => $barcode
                    ])
                    ->view('pages.user.email.ppdb-email')
                    );
    }
      
    }

   
    public function build()
    {
        // return $this->view('view.name');
    }
}
