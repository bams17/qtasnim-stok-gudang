<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    protected $table = 'm_user';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    protected $fillable = [
        'id',
        'email',
      ];

   public function getJWTIdentifier()
      {
          return $this->getKey();
      }
  
      /**
       * Return a key value array, containing any custom claims to be added to the JWT.
       *
       * @return array
       */
      public function getJWTCustomClaims()
      {
          return [];
      }
}
