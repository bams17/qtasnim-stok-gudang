<?php

namespace App\Http\Controllers\Admin\Harga;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Hash;
use Str;
use File;
use Storage;
use DB;
use URL;
use Illuminate\Support\Facades\Redirect;

class hargaController extends Controller
{
    public function index(Type $var = null)
    {
        # code...
        $harga = 'active';
        $produk = DB::table('m_produk')
                     ->get();
        return view('pages.admin.harga.manage-harga', compact('harga', 'produk'));
    }

    function dataTabel()
    {
        $data = DB::table('m_harga')
                    ->select('m_harga.*', 'm_produk.*')
                    ->leftJoin('m_produk','m_produk.kode_produk', 'm_harga.kode_produk')
                    ->get();

        return response()->json($data);
        
    }

    public function store(Request $request)
    {
        $messages = [
            'kode_produk' => ':attribute, data tidak boleh kosong',
            'harga_produk' => ':attribute '.$request->album.' sudah digunakan/sudah ada',
        ];

        $rules =  [
            'kode_produk' => 'required',
            'harga_produk' => 'required',
            'satua_produk' => 'required'
        ];

        $validator = Validator::make($request->all(),$rules
       ,$messages);
     
        if($validator->fails()){ 
            $response = array(
                "status" => false,
                "data" => $validator->errors()->first()
            );
            return response()->json($response);
        }

        DB::beginTransaction();

        try {

            $exist =  DB::table('m_harga')
                        ->where('kode_produk', $request->kode_produk)
                        ->exists();

            if($exist){
                DB::table('m_harga')
                    ->where('kode_produk', $request->kode_produk)
                    ->update([
                        'harga_produk' => $request->harga_produk,
                        'satua_produk' => $request->satua_produk,
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    ]);
            }else{
                DB::table('m_harga')->insert([
                    'kode_produk' => $request->kode_produk,
                    'harga_produk' => $request->harga_produk,
                    'satua_produk' => $request->satua_produk,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                ]);
            }


            DB::table('m_log_harga')->insert([
                'kode_produk' => $request->kode_produk,
                'harga_produk' => $request->harga_produk,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

            
            
            DB::commit();

            $response = array(
                "status" =>true,
                "data" => "Harga berhasil ditambahkan"
            );



        } catch (\Exception $e) {

            DB::rollback();
            $response = array(
                "status" =>false,
                "data" => $e->getMessage()
            );

        }

        return response()->json($response);

    }

    public function update(Request $request)
    {
        $messages = [
            'kode_produk' => ':attribute, data tidak boleh kosong',
            'harga_produk' => ':attribute '.$request->album.' sudah digunakan/sudah ada',
        ];

        $rules =  [
            'kode_produk' => 'required',
            'harga_produk' => 'required',
            'satua_produk' => 'required'
        ];

        $validator = Validator::make($request->all(),$rules
       ,$messages);
     
        if($validator->fails()){ 
            $response = array(
                "status" => false,
                "data" => $validator->errors()->first()
            );
            return response()->json($response);
        }

        DB::beginTransaction();

        try {

            $exist =  DB::table('m_harga')
                        ->where('kode_produk', $request->kode_produk)
                        ->exists();
                        
            DB::table('m_harga')
                ->where('kode_produk', $request->kode_produk)
                ->update([
                    'harga_produk' => $request->harga_produk,
                    'satua_produk' => $request->satua_produk,
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                ]);


            DB::table('m_log_harga')->insert([
                'kode_produk' => $request->kode_produk,
                'harga_produk' => $request->harga_produk,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

            
            
            DB::commit();

            $response = array(
                "status" =>true,
                "data" => "Harga berhasil ditambahkan"
            );



        } catch (\Exception $e) {

            DB::rollback();
            $response = array(
                "status" =>false,
                "data" => $e->getMessage()
            );

        }

        return response()->json($response);

    }
}
