<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Analytics;
use Spatie\Analytics\Period;
use Carbon\Carbon;

class adminController extends Controller
{
   
    public function index()
    {
        //
        $startDate = Carbon::now()->subYear();
        $endDate = Carbon::now();

        $pages_visit = Analytics::fetchMostVisitedPages(Period::days(7));

        
        $visitors = Analytics::fetchVisitorsAndPageViews(Period::days(7));

        $visitors_now = Analytics::fetchVisitorsAndPageViews(Period::days(0));

      
        $total_visitors = Analytics::fetchTotalVisitorsAndPageViews(Period::days(7));

        
        $top_referrers = Analytics::fetchTopReferrers(Period::days(7));

        
        $user_types = Analytics::fetchUserTypes(Period::days(7));

       
        $top_browser = Analytics::fetchTopBrowsers(Period::days(7));

        // dd($top_browser);

        $pages_top = Analytics::performQuery(
            Period::years(1),
               'ga:sessions',
               [
                'dimensions'=>'ga:pageTitle,ga:pagePath',
                'metrics'=> 'ga:pageviews,ga:uniquePageviews',
                'sort'=>'-ga:pageviews',
               ]
         )->rows;

         $browser_top = Analytics::performQuery(
            Period::years(1),
               'ga:sessions',
               [
                'dimensions'=>'ga:browser',
                'metrics'=>'ga:sessions'
               ]
         )->rows;

        $users_pageviews = Analytics::performQuery(
            Period::years(1),
            'ga:sessions',
               [
                'metrics'=>'ga:pageviews'
               ]
         )->rows;

         $users_type = Analytics::performQuery(
            Period::years(1),
            'ga:sessions',
               [
                'dimensions'=>'ga:userType',
                'metrics'=>'ga:sessions'
               ]
         )->rows;


        //  dd($users_type);


        return view('pages.admin.home',compact('pages_visit','pages_top', 'visitors', 'total_visitors', 'top_referrers', 'user_types', 'browser_top','visitors_now','users_pageviews','users_type') );
    }

    
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

   
    public function show($id)
    {
        //
    }

   
   
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

   
    public function destroy($id)
    {
        //
    }
}
