<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Analytics;
use Spatie\Analytics\Period;
use Carbon\Carbon;
use Str;
use File;
use DB;
use Mail;
use Hash;


class NotificationController extends Controller
{

    public function AktifasiUser($type, $token, $pesan, $titel)
    {
        # code...

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://fcm.googleapis.com/fcm/send',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
        "to" : "'.$token.'",
        "collapse_key" : "type_a",
        "notification" : {
            "body" : "'.$pesan.'",
            "title": "'.$titel.'"
        },
        "data" : {
            "code" : " ",
            "type" : "'.$type.'"
        }
        }',
        CURLOPT_HTTPHEADER => array(
            'Authorization: key=AAAA8RtvMU8:APA91bGqT9ONVHMGPKQXX59imlzo8UoQzhlyGK7doFPHOE13gWnvFyf759JJ5tsiUnCw8GGrns7hBwu-cdgwWPbr-84H4tbVlAXz-je3areKrPEp3GmmNNi4ZWxwdvcwyPM8Fe3wc6Iu',
            'Content-Type: application/json'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

    }

    public function StatusLaporan($type, $token, $pesan, $titel, $code)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://fcm.googleapis.com/fcm/send',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
        "to" : "'.$token.'",
        "collapse_key" : "type_a",
        "notification" : {
            "body" : "'.$pesan.'",
            "title": "'.$titel.'"
        },
        "data" : {
            "code" : "'.$code.'",
            "type" : "'.$type.'"
        }
        }',
        CURLOPT_HTTPHEADER => array(
            'Authorization: key=AAAA8RtvMU8:APA91bGqT9ONVHMGPKQXX59imlzo8UoQzhlyGK7doFPHOE13gWnvFyf759JJ5tsiUnCw8GGrns7hBwu-cdgwWPbr-84H4tbVlAXz-je3areKrPEp3GmmNNi4ZWxwdvcwyPM8Fe3wc6Iu',
            'Content-Type: application/json'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
        // dd($response);
    }

    public function SendwhatsApp($nomor, $pesan)
    {
        
        $data = DB::table('m_device')->first();

        $nomorsend = preg_replace('/^0/','62',$nomor);
        // dd($data);
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://send.bambangyudinugraha.icu/send-media',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
            "number" : "'.$nomorsend.'",
            "caption" : "'.$pesan.'",
            "file" : "'.env('IMAGE_SEND').'"
        }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);


    }

}