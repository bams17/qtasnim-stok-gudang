<?php

namespace App\Http\Controllers\Admin\Produk;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Hash;
use Str;
use File;
use Storage;
use DB;
use URL;

class produkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $harga = 'active';
        return view('pages.admin.harga.manage-produk', compact('harga'));
    }
    function dataTabel()
    {
        $data = DB::table('m_produk')
                    ->get();

        return response()->json($data);
        
    }
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
        $messages = [
            'kode_produk' => ':attribute, data tidak boleh kosong',
            'nama_varietas' => ':attribute '.$request->album.' sudah digunakan/sudah ada',
        ];

        $rules =  [
            'kode_produk' => 'required',
            'nama_varietas' => 'required',
        ];

        $validator = Validator::make($request->all(),$rules
       ,$messages);
     
        if($validator->fails()){ 
            $response = array(
                "status" => false,
                "data" => $validator->errors()->first()
            );
            return response()->json($response);
        }

        try {
            DB::table('m_produk')
                ->insert([
                    "kode_produk" => $request->kode_produk,
                    "link_gambar" => $this->uploadGambar($request->gambar),
                    "nama_produk" => $request->nama_varietas,
                    "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                ]);

            DB::table('m_stok')
                ->insert([
                    "kode_produk" => $request->kode_produk,
                    "stok_barang" => 0,
                    "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                ]);

            DB::table('m_harga')->insert([
                    'kode_produk' => $request->kode_produk,
                    'harga_produk' => 0,
                    'satua_produk' => "-",
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                ]);
                
            $response = array(
                "status" =>true,
                "data" => "Berhasil menambahkan Produk baru"
            );
        } catch (\Exception $e) {
            $response = array(
                "status" =>false,
                "data" => "Error, ". $e->getMessage()
            );
            
        }
        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $messages = [
            'kode_produk' => ':attribute, data tidak boleh kosong',
            'nama_varietas' => ':attribute '.$request->album.' sudah digunakan/sudah ada',
        ];

        $rules =  [
            'kode_produk' => 'required',
            'nama_varietas' => 'required',
        ];

        $validator = Validator::make($request->all(),$rules
       ,$messages);
     
        if($validator->fails()){ 
            $response = array(
                "status" => false,
                "data" => $validator->errors()->first()
            );
            return response()->json($response);
        }

        // dd($request->gambar);
        try {
            if($request->gambar){
                DB::table('m_produk')
                ->where('kode_produk', $request->kode_produk)
                ->update([
                    "link_gambar" => $this->uploadGambar($request->gambar),
                    "nama_produk" => $request->nama_varietas,
                    "updated_at" => Carbon::now()->format('Y-m-d H:i:s')
                ]);
            }else{
                DB::table('m_produk')
                ->where('kode_produk', $request->kode_produk)
                ->update([
                    "nama_produk" => $request->nama_varietas,
                    "updated_at" => Carbon::now()->format('Y-m-d H:i:s')
                ]);
            }
            
            $response = array(
                "status" =>true,
                "data" => "Berhasil update Produk baru ". $request->nama_varietas
            );
        } catch (\Exception $e) {
            $response = array(
                "status" =>false,
                "data" => "Error, ". $e->getMessage()
            );
            
        }
        return response()->json($response);
    }

    
    public function destroy(Request $request)
    {
        //
        // dd($request->all());
        try {
            //code...
            DB::table('m_produk')
                ->where('id', $request->id)
                ->delete();
            $response = array(
                    "status" =>true,
                    "data" => "Berhasil dihapus Produk baru"
            );

        } catch (\Exception $e) {
            //throw $th;
            $response = array(
                "status" =>false,
                "data" => "Error, ". $e->getMessage()
            );

            return response()->json($response);
        }
       

    }

    public function uploadGambar($foto)
    {
      $random = Str::random(15);
      $storageIklan = public_path('/storage/foto');
      if(!File::isDirectory($storageIklan))
      {

        File::makeDirectory($storageIklan, 0777, true, true); 

        $saveFoto =  Image::make($foto)->save($storageIklan.'/'.$random.str_replace("image/",".",$foto->getMimeType()));
       
        if($saveFoto){
          $result = '/storage/foto/'.$random.str_replace("image/",".",$foto->getMimeType());
          return $result;
        }
      }
      else
      {
        $saveFoto =  Image::make($foto)->save($storageIklan.'/'.$random.str_replace("image/",".",$foto->getMimeType()));
        if($saveFoto){
          $result = '/storage/foto/'.$random.str_replace("image/",".",$foto->getMimeType());
          return $result;
        }
      }
    }

}
