<?php

namespace App\Http\Controllers\Admin\Stok;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Hash;
use Str;
use File;
use Storage;
use DB;
use URL;

class stokController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $total_produk = DB::table('m_produk')->count();
        $total_inbond = DB::table('m_transaksi_barang')->where('m_transaksi_barang.tipe', 1)->count();
        $total_outbond = DB::table('m_transaksi_barang')->where('m_transaksi_barang.tipe', 0)->count();

        $populer =  (object)array(
                    "data_populer" => DB::table('t_transaksi_barang')
                                            ->select(DB::raw('sum(t_transaksi_barang.jumlah) as total'), 
                                                     'm_produk.nama_produk as nama',
                                                     'm_produk.link_gambar as link',
                                                     )
                                            ->leftJoin('m_produk', 'm_produk.kode_produk','t_transaksi_barang.kode_barang')
                                            ->leftJoin('m_transaksi_barang', 'm_transaksi_barang.kode_transaksi', 't_transaksi_barang.kode_transaksi')
                                            ->where('m_transaksi_barang.tipe', 0)
                                            ->groupBy('t_transaksi_barang.kode_barang')
                                            ->orderBy('t_transaksi_barang.kode_barang')
                                            ->get()
                                );
        $harga = 'active';
        return view('pages.admin.stok.manage-stok', compact('harga','populer', 'total_produk', 'total_inbond','total_outbond'));
    }

    function dataTabelStok()
    {
        $data = DB::table('m_stok')
                    ->select('m_stok.stok_barang', 
                             'm_produk.*',
                             'm_harga.satua_produk',
                             \DB::raw('(SELECT SUM(t_transaksi_barang.jumlah) 
                                        FROM `t_transaksi_barang` 
                                        LEFT JOIN m_transaksi_barang ON m_transaksi_barang.kode_transaksi = t_transaksi_barang.kode_transaksi 
                                        WHERE m_transaksi_barang.tipe = 0 AND t_transaksi_barang.kode_barang = m_stok.kode_produk) AS out_bond'))
                    ->leftJoin('m_produk','m_produk.kode_produk', 'm_stok.kode_produk')
                    ->leftJoin('m_harga','m_harga.kode_produk', 'm_stok.kode_produk')
                    ->get();

        return response()->json($data);
        
    }

    function dataTabelTransaksi(Request $request)
    {
        DB::statement(DB::raw('set @no=0'));
        DB::statement(DB::raw('set @nomor=0'));

        $data_in = DB::table('t_transaksi_barang')
                        ->select( \DB::raw('@no:=@no+1 as no'),'t_transaksi_barang.*')
                        ->leftJoin('m_transaksi_barang', 'm_transaksi_barang.kode_transaksi', 't_transaksi_barang.kode_transaksi')
                        ->where('m_transaksi_barang.tipe', 1)
                        ->where('t_transaksi_barang.kode_barang', $request->kode)
                        ->get();

        $data_out = DB::table('t_transaksi_barang')
                        ->select( \DB::raw('@nomor:=@nomor+1 as no'),'t_transaksi_barang.*')
                        ->leftJoin('m_transaksi_barang', 'm_transaksi_barang.kode_transaksi', 't_transaksi_barang.kode_transaksi')
                        ->where('m_transaksi_barang.tipe', 0)
                        ->where('t_transaksi_barang.kode_barang', $request->kode)
                        ->get();
        
        $response = array(
                "status" =>true,
                "data_in" => $data_in,
                "data_out" => $data_out
            );

        return response()->json($response);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // INBOND BARANG

    public function indexInBond()
    {
        //
        $harga = 'active';
        $produk = DB::table('m_produk')
                    ->select('m_produk.nama_produk',
                             'm_produk.kode_produk',
                             'm_harga.harga_produk',
                             'm_harga.satua_produk',
                             'm_stok.stok_barang')
                    ->leftJoin('m_harga','m_harga.kode_produk', 'm_produk.kode_produk')
                    ->leftJoin('m_stok','m_stok.kode_produk', 'm_produk.kode_produk')
                    ->get();
        return view('pages.admin.stok.manage-inbond', compact('harga', 'produk'));
    }

    public function storeInbond(Request $request)
    {
        //
        // dd($request->all());
        DB::beginTransaction();
        try {
            //code...
            $count_code = str_pad(DB::table('m_transaksi_barang')->count(), 4, '0', STR_PAD_LEFT);
            $random_code = strtoupper(Str::random(4));
            $kode_transaksi = 'INV-IN-'.$random_code."-".$count_code;

            DB::table('m_transaksi_barang')
            ->insert([
                "kode_transaksi" => $kode_transaksi,
                "tipe" => $request->tipe_transaksi,
                "created_at" => Carbon::now()->format('Y-m-d H:i:s')
            ]);

            foreach ($request->produk as $row) {
                // dd($row['kode']);

                DB::table('t_transaksi_barang')
                    ->insert([
                        "kode_transaksi" => $kode_transaksi,
                        "kode_barang" =>$row['kode'],
                        "jumlah" => $row['jumlah'],
                        "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                DB::table('m_stok')
                    ->where('kode_produk', $row['kode'])
                    ->increment('stok_barang', $row['jumlah']);
            }
           

            DB::commit();
            $response = array(
                "status" =>true,
                "data" => "Berhasil menambahkan Produk baru"
            );

        } catch (\Exception $e) {

            DB::rollback();
            $response = array(
                "status" =>false,
                "data" => "Error, ". $e->getMessage()
            );
            //throw $th;
        }

        return response()->json($response);
    }

    function updateInBond(Request $request)
    {
        // dd($request->all());
        DB::beginTransaction();
        try {
            //code...

            DB::table('m_transaksi_barang')
                ->where('kode_transaksi', $request->kode_transaksi)
                ->update([
                    "updated_at" => Carbon::now()->format('Y-m-d H:i:s')
                ]);
            
            $data_before = DB::table('t_transaksi_barang')
                                ->where('t_transaksi_barang.kode_transaksi', $request->kode_transaksi)
                                ->get();
            
            foreach ($data_before as $row) {
                DB::table('m_stok')
                    ->where('kode_produk', $row->kode_barang)
                    ->decrement('stok_barang', $row->jumlah);
            }

            DB::table('t_transaksi_barang')
                ->where('t_transaksi_barang.kode_transaksi', $request->kode_transaksi)
                ->delete();


            foreach ($request->produk as $row) {
            
                DB::table('t_transaksi_barang')
                    ->insert([
                        "kode_transaksi" => $request->kode_transaksi,
                        "kode_barang" =>$row['kode'],
                        "jumlah" => $row['jumlah'],
                        "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                DB::table('m_stok')
                    ->where('kode_produk', $row['kode'])
                    ->increment('stok_barang', $row['jumlah']);
            }
           

            DB::commit();
            $response = array(
                "status" =>true,
                "data" => "Berhasil update transaksi Inbound"
            );

        } catch (\Exception $e) {

            DB::rollback();
            $response = array(
                "status" =>false,
                "data" => "Error, ". $e->getMessage()
            );
            //throw $th;
        }

        return response()->json($response);

    }

    function dataTabelInBond()
    {
        $data = DB::table('m_transaksi_barang')
                        ->where('tipe', 1)
                        ->get();

        return response()->json($data);
        
    }

    function detailInBond(Request $request) {
       $data = DB::table('t_transaksi_barang')
                        ->leftJoin('m_produk','m_produk.kode_produk','t_transaksi_barang.kode_barang')
                        ->leftJoin('m_harga','m_harga.kode_produk','m_produk.kode_produk')
                        ->leftJoin('m_stok','m_stok.kode_produk', 'm_produk.kode_produk')
                        ->where('t_transaksi_barang.kode_transaksi', $request->kode)
                        ->get();

        return response()->json($data);
    }


    // Outbound

    public function indexOutBond()
    {
        //
        $harga = 'active';
        $produk = DB::table('m_produk')
                    ->select('m_produk.nama_produk',
                             'm_produk.kode_produk',
                             'm_harga.harga_produk',
                             'm_harga.satua_produk',
                             'm_stok.stok_barang')
                    ->leftJoin('m_harga','m_harga.kode_produk', 'm_produk.kode_produk')
                    ->leftJoin('m_stok','m_stok.kode_produk', 'm_produk.kode_produk')
                    ->get();
        return view('pages.admin.stok.manage-outbond', compact('harga', 'produk'));
    }

    public function storeOutBond(Request $request)
    {
        //
        // dd($request->all());
        DB::beginTransaction();
        try {
            //code...
            $count_code = str_pad(DB::table('m_transaksi_barang')->count(), 4, '0', STR_PAD_LEFT);
            $random_code = strtoupper(Str::random(4));
            $kode_transaksi = 'INV-OUT-'.$random_code."-".$count_code;

            DB::table('m_transaksi_barang')
            ->insert([
                "kode_transaksi" => $kode_transaksi,
                "tipe" => $request->tipe_transaksi,
                "created_at" => Carbon::now()->format('Y-m-d H:i:s')
            ]);

            foreach ($request->produk as $row) {
                // dd($row['kode']);

                DB::table('t_transaksi_barang')
                    ->insert([
                        "kode_transaksi" => $kode_transaksi,
                        "kode_barang" =>$row['kode'],
                        "jumlah" => $row['jumlah'],
                        "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                DB::table('m_stok')
                    ->where('kode_produk', $row['kode'])
                    ->decrement('stok_barang', $row['jumlah']);
            }
           

            DB::commit();
            $response = array(
                "status" =>true,
                "data" => "Berhasil menambahkan Produk baru"
            );

        } catch (\Exception $e) {

            DB::rollback();
            $response = array(
                "status" =>false,
                "data" => "Error, ". $e->getMessage()
            );
            //throw $th;
        }

        return response()->json($response);
    }

    function updateOutBond(Request $request)
    {
        // dd($request->all());
        DB::beginTransaction();
        try {
            //code...

            DB::table('m_transaksi_barang')
                ->where('kode_transaksi', $request->kode_transaksi)
                ->update([
                    "updated_at" => Carbon::now()->format('Y-m-d H:i:s')
                ]);
            
            $data_before = DB::table('t_transaksi_barang')
                                ->where('t_transaksi_barang.kode_transaksi', $request->kode_transaksi)
                                ->get();
            
            foreach ($data_before as $row) {
                DB::table('m_stok')
                    ->where('kode_produk', $row->kode_barang)
                    ->increment('stok_barang', $row->jumlah);
            }

            DB::table('t_transaksi_barang')
                ->where('t_transaksi_barang.kode_transaksi', $request->kode_transaksi)
                ->delete();


            foreach ($request->produk as $row) {
            
                DB::table('t_transaksi_barang')
                    ->insert([
                        "kode_transaksi" => $request->kode_transaksi,
                        "kode_barang" =>$row['kode'],
                        "jumlah" => $row['jumlah'],
                        "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                    ]);

                DB::table('m_stok')
                    ->where('kode_produk', $row['kode'])
                    ->decrement('stok_barang', $row['jumlah']);
            }
           

            DB::commit();
            $response = array(
                "status" =>true,
                "data" => "Berhasil update transaksi Inbound"
            );

        } catch (\Exception $e) {

            DB::rollback();
            $response = array(
                "status" =>false,
                "data" => "Error, ". $e->getMessage()
            );
            //throw $th;
        }

        return response()->json($response);

    }

    function dataTabelOutBond()
    {
        $data = DB::table('m_transaksi_barang')
                    ->where('tipe', 0)
                    ->get();

        return response()->json($data);
        
    }

    function detailIOutBond(Request $request) {
       $data = DB::table('t_transaksi_barang')
                ->leftJoin('m_produk','m_produk.kode_produk','t_transaksi_barang.kode_barang')
                ->leftJoin('m_harga','m_harga.kode_produk','m_produk.kode_produk')
                ->where('t_transaksi_barang.kode_transaksi', $request->kode)
                ->get();

        return response()->json($data);
    }

}
