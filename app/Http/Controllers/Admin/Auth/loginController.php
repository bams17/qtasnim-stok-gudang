<?php

namespace App\Http\Controllers\admin\auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class loginController extends Controller
{
    
    public function index()
    {
        
        if (Auth::guard('admin')->check()) {
            return redirect('/admin/dashboard/manage-stok-barang');
        }else{
            return view('pages.admin.auth.login');
        }
        
    }

    public function Login(Request $request)
    {
        $messages = [
            'required' => ':attribute, data tidak boleh kosong',
            'exists'=>'Silahkan memasukan email yang benar',
            'required_with' => ':attribute, komfrimasi password tidak sama',
            'unique'=> ':attribute, sudah terdaftar silahkan menggunakan email yang lain'
        ];
        $rules =  [
            'email' => 'required|exists:admin,email',
            'password' => 'required'
        ];

        $validator = Validator::make($request->all(),$rules
        ,$messages);
      
         if($validator->fails()){ 
             return redirect('/admin/auth/login')
                             ->withErrors($validator)
                             ->withInput();
         }

        if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], true)){

              return redirect('admin/dashboard/manage-stok-barang');

        }else{

              return redirect('/admin');

        }      


    }

    public function Logout(Request $request)
    {
        
        if(Auth::guard('admin')->logout()){
            return view('pages.admin.auth.login');
        }else{
            return redirect('/admin'); 
        }
        
    }

    
    
}
