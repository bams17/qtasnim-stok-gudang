<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Hash;
use Str;
use File;
use Storage;
use DB;
use URL;

class registrasiController extends Controller
{
   
    public function index(Request $request)
    {
        $dashboard = 'active';
        $data_admin = DB::table('admin')
                    ->get();
        if($request->data!=null){
            // dd("DSAD");
            $request->session()->flash('success','Berhasil menambahkan admin baru !'); 
            return view('pages.admin.auth.tambah-admin', compact('dashboard','data_admin'));
        }else {
            return view('pages.admin.auth.tambah-admin', compact('dashboard','data_admin'));
        }
        
    }

    
    public function create()
    {
        $dashboard = 'active';
        return view('pages.admin.auth.create-admin', compact('dashboard'));
        
    }

    
    public function store(Request $request)
    {
    
        $messages = [
            'required' => ':attribute, data tidak boleh kosong',
            'exists'=>'agent id tidak ada dalam keranjang pengembalian',
            'required_with' => ':attribute, komfrimasi password tidak sama',
            'unique'=> ':attribute, sudah terdaftar silahkan menggunakan email yang lain'
        ];
        $rules =  [
            'email' => 'required|email|unique:admin',
            'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:6',
            'link_foto'=>'required'
        ];

        $validator = Validator::make($request->all(),$rules
       ,$messages);
     
        if($validator->fails()){ 
            $response = array(
                "status" => false,
                "data" => $validator->errors()->first()
            );
            return response()->json($response);
            // return redirect()->back()->with('erorr', $validator->errors()->first())->withInput($request->all());
        }

        $insert = array(
            "nama" => $request->nama,
            "email" => $request->email,
            "password" => Hash::make($request->password),
            "link_foto" => $this->uploadGambar($request->link_foto),
            "created_at" => Carbon::now()->format('Y-m-d H:i:s')
        );

        try {
            DB::table('admin')
                ->insert($insert);
                $data = "Berhasil menambahkan pengelola admin baru";
                $response = array(
                    "status" => true,
                    "data" => $data
                );
        } catch (\Exception $e) {
                $data = $e->getMessage();
                $response = array(
                    "status" => false,
                    "data" => $data
                );
        }

        return response()->json($response);
        

    }

    
    public function show($id)
    {
        //
    }

    
    public function edit(Request $request)
    {
        $messages = [
            'required' => ':attribute, data tidak boleh kosong',
            'exists'=>'agent id tidak ada sistem keranjang pengembalian',
            'required_with' => ':attribute, komfrimasi password tidak sama',
            'unique'=> ':attribute, sudah terdaftar silahkan menggunakan email yang lain'
        ];
        $rules =  [
            'id' => 'required|exists:admin,id'
        ];

        $validator = Validator::make($request->all(),$rules
       ,$messages);
     
        if($validator->fails()){ 
            return redirect()->with('error', $validator->errors()->first());
                            
        }

        try {
            $admin = DB::table('admin')
                    ->where('id', $request->id)
                    ->first();
        } catch (\Exception $e) {
                $admin = $e->getMessage();
        }
        return view('pages.admin.auth.update-admin', compact('admin'));
    }

   
    public function update(Request $request, $id)
    {

        // dd($request->all());

        $messages = [
            'required' => ':attribute, data tidak boleh kosong',
            'exists'=>'agent id tidak ada dalam keranjang pengembalian',
            'required_with' => ':attribute, komfrimasi password tidak sama',
            'unique'=> ':attribute, sudah terdaftar silahkan menggunakan email yang lain'
        ];

        if($request->password!=null){
            $rules =  [
                'nama' => 'required',
                'email' => 'required',
                'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
                'password_confirmation' => 'min:6',
            ];

            if($request->link_foto!=null) {
                $insert = array(
                    "nama" => $request->nama,
                    "email" => $request->email,
                    "password" => Hash::make($request->password),
                    "link_foto" => $this->uploadGambar($request->link_foto),
                    "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                );
            }else {
                $insert = array(
                    "nama" => $request->nama,
                    "email" => $request->email,
                    "password" => Hash::make($request->password),
                    "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                );
            }

        }else{
            $rules =  [
                'nama' => 'required',
                'email' => 'required',
            ];

            if($request->link_foto!=null) {
                $insert = array(
                    "nama" => $request->nama,
                    "email" => $request->email,
                    "link_foto" => $this->uploadGambar($request->link_foto),
                    "updated_at" => Carbon::now()->format('Y-m-d H:i:s')
                );
            }else {
                $insert = array(
                    "nama" => $request->nama,
                    "email" => $request->email,
                    "updated_at" => Carbon::now()->format('Y-m-d H:i:s')
                );
            }
        }

        $validator = Validator::make($request->all(),$rules
       ,$messages);
     
        if($validator->fails()){ 
            return redirect('/admin/dashboard/manage-admin/edit?id='.$id)
                            ->withErrors($validator)
                            ->withInput();
        }

        try {
                DB::table('admin')
                        ->where('id', $id)
                        ->update($insert);
                $data = "Data admin ". $request->nama." berhasil diupdate";
                
        } catch (\Exception $e) {
                $data = "Error ". $e->getMessage();
        }

        $dashboard = 'active';
        $data_admin = DB::table('admin')
                    ->get();
        return view('pages.admin.auth.tambah-admin', compact('data','dashboard','data_admin'))->with("success", $data);
       
    }

   
    public function destroy($id)
    {
        //
    }

    public function uploadGambar($foto)
    {
      $random = Str::random(15);
      $storageIklan = public_path('/storage/administrator');
      if(!File::isDirectory($storageIklan))
      {

        File::makeDirectory($storageIklan, 0777, true, true); 

        $saveFoto =  Image::make($foto)->save($storageIklan.'/'.$random.str_replace("image/",".",$foto->getMimeType()));
       
        if($saveFoto){
          $result = '/storage/iklan/'.$random.str_replace("image/",".",$foto->getMimeType());
          return $result;
        }
      }
      else
      {
        $saveFoto =  Image::make($foto)->save($storageIklan.'/'.$random.str_replace("image/",".",$foto->getMimeType()));
        if($saveFoto){
          $result = '/storage/administrator/'.$random.str_replace("image/",".",$foto->getMimeType());
          return $result;
        }
      }
    }

}
