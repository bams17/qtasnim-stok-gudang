<?php

namespace App\Http\Controllers\API\Produk;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Str;
use File;
use DB;
use Mail;
use Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Http\Controllers\Admin\NotificationController;

class produkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('m_harga')
                    ->select('m_harga.*', 'm_produk.*')
                    ->leftJoin('m_produk','m_produk.kode_produk', 'm_harga.kode_produk')
                    ->get();

        $arrayData = array();

        for ($i=0; $i < count($data); $i++) { 

            $log = DB::table('m_log_harga')
                        ->where('kode_produk', $data[$i]->kode_produk)
                        ->orderby('created_at', 'desc')
                        ->limit(2)
                        ->get();
            if(count($log)>= 2 ){
                $objekData = (object) [
                    "gambar" => url('/') .$data[$i]->link_gambar,
                    "nama_produk" => $data[$i]->nama_produk,
                    "harga_produk" => $data[$i]->harga_produk,
                    "Satua_produk" => $data[$i]->satua_produk,
                    "selisih_haga_sebelum" => abs($log[1]->harga_produk - $data[$i]->harga_produk),
                    "presentase_status_naik" => $log[1]->harga_produk > $data[$i]->harga_produk ? false : true,
                    "presentase_harga" => abs(ceil(($log[1]->harga_produk - $data[$i]->harga_produk)/$log[1]->harga_produk * 100 ))." %"
                ];
            }else{
                $objekData = (object) [
                    "gambar" =>url('/') .$data[$i]->link_gambar,
                    "nama_produk" => $data[$i]->nama_produk,
                    "harga_produk" => $data[$i]->harga_produk,
                    "Satua_produk" => $data[$i]->satua_produk,
                    "selisih_haga_sebelum" => abs($log[0]->harga_produk - $data[$i]->harga_produk),
                    "presentase_status_naik" => $log[0]->harga_produk > $data[$i]->harga_produk ? true : false,
                    "presentase_harga" => abs(ceil(($log[0]->harga_produk - $data[$i]->harga_produk)/$log[0]->harga_produk * 100 ))." %"
                ];
            }

            array_push($arrayData, $objekData);
        }

        $response = array(
            "status" =>true,
            "massage" => "list harga terkini",
            "data" => $arrayData
        );


        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
