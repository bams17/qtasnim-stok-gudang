<?php

namespace App\Http\Controllers\API\News;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Str;
use File;
use DB;
use Mail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Admin\NotificationController;

class NewsController extends Controller
{
    public function __construct()
    {
    //   $this->middleware('jwt.verify');
    }

    //
    public function kategori(Type $var = null)
    {
         $data_konten = DB::table('m_kategori_konten')
                            ->get();

        return response()->json(['status'=>true, 'message' => 'kategori konten', 'data'=> $data_konten]);
    }
    public function detailkategori(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'kategori_id' => 'required',
        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        $data_konten = DB::table('m_konten')
                ->select('m_konten.judul',
                        'm_konten.penulis',
                        'm_konten.durasi_baca',
                        'm_konten.id',
                        'm_konten.created_at',
                        'm_konten.publish',
                        'm_konten.link_foto',
                        'm_konten.kategori_id',
                        'm_konten.caption_foto',
                        'm_kategori_konten.kategori')
                ->leftJoin('m_kategori_konten','m_kategori_konten.id','m_konten.kategori_id')
                ->where('m_konten.kategori_id',$request->kategori_id)
                ->orderBy('m_konten.id','desc')
                ->get();

        return response()->json(['status'=>true, 'message' => 'detail konten', 'data'=> $data_konten]);
    }

    public function detailkonten(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'slug' => 'required',
        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        try {
            //code...

        $data_konten = DB::table('m_konten')
                        ->select('m_konten.judul',
                                'm_konten.penulis',
                                'm_konten.durasi_baca',
                                'm_konten.id',
                                'm_konten.created_at',
                                'm_konten.publish',
                                'm_konten.link_foto',
                                \DB::raw('CONCAT("'.url('/').'",m_konten.link_foto) AS link_foto'),
                                'm_konten.konten',
                                'm_konten.kategori_id',
                                'm_konten.caption_foto',
                                'm_kategori_konten.kategori','m_seo.slug',
                                'm_seo.tags',
                                'm_seo.meta_description')
                        ->leftJoin('m_seo','m_seo.id','m_konten.seo_id')
                        ->leftJoin('m_kategori_konten','m_kategori_konten.id','m_konten.kategori_id')
                        ->where('m_seo.slug',$request->slug)
                        ->first();

                return response()->json(['status'=>true, 'message' => 'detail konten', 'data'=> $data_konten]);
        } catch (\Exception $e) {
            //throw $th;
                return response()->json(['status'=>false, 'error' => 'detail konten', 'data'=> $$e]);
        }

    }

    public function kontens(Request $request)
    {

        $limit = $request->input('limit', 4);
        $offset = $request->input('offset', 0);
        $kategori = $request->input('kategori', 'artikel');   

        try {
            $data_konten = DB::table('m_konten')
                ->select('m_konten.judul',
                        'm_konten.penulis',
                        'm_konten.durasi_baca',
                        'm_konten.id',
                        'm_konten.created_at',
                        'm_konten.publish',
                        'm_konten.link_foto',
                        \DB::raw('CONCAT("'.url('/').'",m_konten.link_foto) AS link_foto'),
                        'm_konten.kategori_id',
                        'm_konten.caption_foto',
                        'm_kategori_konten.kategori',
                        'm_seo.slug',
                        'm_seo.tags',
                        'm_seo.meta_description')
                ->leftJoin('m_seo','m_seo.id','m_konten.seo_id')
                ->leftJoin('m_kategori_konten','m_kategori_konten.id','m_konten.kategori_id')
                ->whereNull('m_konten.sub_konten_id')
                ->orderBy('m_konten.id','desc')
                ->where('m_kategori_konten.kategori', $kategori)
                ->limit($limit)
                ->offset($offset)
                ->get();

            return response()->json(['status'=>true, 'message' => 'list konten', 'data'=> $data_konten]);
        } catch (\Exception $e) {
            //throw $th;
            return response()->json(['status'=>false, 'message' => 'list konnten', 'data'=> $e->getMessage()]);
        }
    
    }

    public function userTim(Type $var = null)
    {
        # code...
        $data = DB::table('m_user')
                    ->select('m_user.nama',
                              'm_user.link_gambar', 
                              'm_wilayah_kerja.kota',
                              'm_wilayah_kerja.kec',
                              \DB::raw('(SELECT COUNT(*) as total FROM `m_canvasser` WHERE m_canvasser.uuid_user = m_user.uuid) AS target'),
                              )
                    ->leftJoin('m_wilayah_kerja', 'm_wilayah_kerja.uuid_user', '=', 'm_user.uuid')
                    ->limit(10)
                    ->orderBy('target', 'DESC')
                    ->get();
                    

        return response()->json(['status'=>true, 'message' => 'data user', 'data'=> $data]);
    }

    public function shareKontenStatus(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'uuid_user' => 'required|exists:m_user,uuid',
        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        try {
            //code...
            DB::table('m_point')
                ->insert([
                    "uuid_user" => $request->uuid_user,
                    "point" => 10
                ]);

        
            $type = 0;

            $token = DB::table('m_token_firebase')
                            ->where('uuid_user', $request->uuid_user)
                            ->value('token');
        
            $titel = "KAMU MENDAPATKAN 10 POINT 🎉";
            $pesan = "Hai ".$request->nama." hasil dari berbagi konten dari kamu sudah ada yang lihat 😎";

            (new NotificationController)->AktifasiUser($type, $token, $pesan, $titel);

            $response = array(
                "status"   => true,
                "message"   => "penambahan point berhasil",
            );

            return response()->json($response);
            
            
        } catch (\Throwable $th) {
            //throw $th;
            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $th->getMessage()
            );
            return response()->json($response);
        }
    }
   

}
