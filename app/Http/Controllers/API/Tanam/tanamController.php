<?php

namespace App\Http\Controllers\API\Tanam;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Str;
use File;
use DB;
use Mail;
use Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Http\Controllers\Admin\NotificationController;

class tanamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'uuid_user' => 'required',
        ));

        if($validator->fails()){
            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        $data = DB::table('m_data_tanam')
                  ->select('m_data_tanam.*',
                            'm_user.*',
                            'm_data_tanam.status as status_data_tanam',
                            'm_informasi_tanam.*',
                            'provinces.prov_name',
                            'cities.city_name',
                            \DB::raw('CASE 
                                            WHEN m_jual_tanam.status = 0 THEN "Menunggu Konfirmasi"
                                            WHEN m_jual_tanam.status = 1 THEN "Pesanan diterima"
                                            WHEN m_jual_tanam.status = 2 THEN "Sedang di Survey"
                                            WHEN m_jual_tanam.status = 3 THEN "Proses Panen"
                                            WHEN m_jual_tanam.status = 4 THEN "Pelunasan"
                                            WHEN m_jual_tanam.status = 5 THEN "Selesai"
                                            WHEN m_jual_tanam.status = 6 THEN "Tolak"
                                        ELSE
                                            "Siap Jual"
                                    END AS status_jual'),
                            'm_nama_lembaga.nama_tim')
                  ->leftJoin('m_user','m_user.uuid', 'm_data_tanam.uuid_user')
                  ->leftJoin('m_nama_lembaga','m_nama_lembaga.id','m_user.id_lembaga')
                  ->leftJoin('m_wilayah_lembaga','m_wilayah_lembaga.lembaga_id','m_nama_lembaga.id')
                  ->leftJoin('m_informasi_tanam', 'm_informasi_tanam.kode_tanam','m_data_tanam.kode_tanam')
                  ->leftJoin('m_lokasi_tanam', 'm_lokasi_tanam.kode_tanam', 'm_data_tanam.kode_tanam')
                  ->leftJoin('m_jual_tanam','m_jual_tanam.kode_tanam', 'm_data_tanam.kode_tanam')
                  ->leftJoin('provinces','provinces.id', 'm_wilayah_lembaga.prov_id')
                  ->leftJoin('cities','cities.id', 'm_wilayah_lembaga.city_id')
                  ->where('m_data_tanam.uuid_user', $request->uuid_user)
                  ->orderBy('m_data_tanam.id', 'desc')
                  ->get();

        if(count($data)>0){
            $response = array(
                "status" => true,
                "massage" => "list data tanam",
                "data" => $data
            );
        }else{
            $response = array(
                "status" => false,
                "massage" => "list data tanam tidak ditemukan",
                "data" => $data
            );
        }
        
        
        return response()->json($response);
    }


    public function listdatatanam(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'uuid_user' => 'required|exists:m_user,uuid|exists:m_data_tanam,uuid_user',
        ));

        if($validator->fails()){
            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        $data = DB::table('m_data_tanam')
                  ->select('m_data_tanam.kode_tanam', 'm_data_tanam.title', )
                  ->where('m_data_tanam.uuid_user', $request->uuid_user)
                  ->where('m_data_tanam.status', 0)
                  ->get();

        if(count($data)>0){
            $response = array(
                "status" => true,
                "massage" => "list data tanam",
                "data" => $data
            );
        }else{
            $response = array(
                "status" => false,
                "massage" => "list data tanam tidak ditemukan",
                "data" => $data
            );
        }
        
        
        return response()->json($response);
    }


  
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'uuid_user' => 'required',
            'title' => 'required',
            'varietas' => 'required',

            'luas_lahan' => 'required',
            'satuan_luas_lahan' => 'required',
            'jumlah_benih' => 'required',
            'satuan_jumlah_benih' => 'required',
            'info_pupuk' => 'required',
            'jumlah_waktu_tanam' => 'required',
            'ket_waktu_tanam' => 'required',

            // 'prov_id' => 'required',
            // 'city_id' => 'required',
            // 'dis_id' => 'required',
            // 'subdis_id' => 'required',
            'komoditas' => 'required',
            'alamat' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',

        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        $count_code = str_pad(DB::table('m_user')->count(), 4, '0', STR_PAD_LEFT);
        $random_code = strtoupper(Str::random(4));
        $code_reg = 'TM-'.$random_code."-".$count_code;

        DB::beginTransaction();

        try {
            //code...
            DB::table('m_data_tanam')
                ->insert([
                    'uuid_user' => $request->uuid_user,
                    'kode_tanam' => $code_reg,
                    'title' => $request->title,
                    'komoditas' => $request->komoditas,
                    'varietas' => $request->varietas,
                    'status' => 0,
                    "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                ]);
            
            DB::table('m_informasi_tanam')
                ->insert([
                    'kode_tanam' => $code_reg,
                    'luas_lahan' => $request->luas_lahan,
                    'satuan_luas_lahan' => $request->satuan_luas_lahan,
                    'jumlah_benih' => $request->jumlah_benih,
                    'satuan_jumlah_benih' => $request->satuan_jumlah_benih,
                    'info_pupuk' => $request->info_pupuk,
                    'jumlah_waktu_tanam' => $request->jumlah_waktu_tanam,
                    'ket_waktu_tanam' => $request->ket_waktu_tanam,
                    "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                ]);

             DB::table('m_lokasi_tanam')
                ->insert([
                    'kode_tanam' => $code_reg,
                    // 'prov_id' => $request->prov_id,
                    // 'city_id' => $request->city_id,
                    // 'dis_id' => $request->dis_id,
                    // 'subdis_id' => $request->subdis_id,
                    'alamat' => $request->alamat,
                    'latitude' => $request->latitude,
                    'longitude' => $request->longitude,
                    "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                ]);
            

            DB::commit();
            $data = "Tambah data tanam berhasil";

            $response = array(
                "status" => true,
                "massage" => $data,
                "data" => $data
            );

            return response()->json($response);

        } catch (\Exception $e) {
            //throw $th;
            DB::rollback();

            $data = $e->getMessage();

            $response = array(
                "status" => false,
                "data" => $data
            );

            return response()->json($response);
        }





    }

    
    public function show(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'kode_tanam' => 'required',
        ));

        if($validator->fails()){
            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }


        $data = DB::table('m_data_tanam')
                ->select('m_data_tanam.*',
                        'm_user.*',
                        'm_informasi_tanam.*',
                        'provinces.prov_name',
                        'cities.city_name',
                        'districts.dis_name',
                        'm_lokasi_tanam.alamat',
                        'm_lokasi_tanam.latitude',
                        'm_lokasi_tanam.longitude',
                        'subdistricts.subdis_name',
                        \DB::raw('CASE 
                                    WHEN m_jual_tanam.status = 0 THEN "Menunggu Konfirmasi"
                                    WHEN m_jual_tanam.status = 1 THEN "Pesanan diterima"
                                    WHEN m_jual_tanam.status = 2 THEN "Sedang di Survey"
                                    WHEN m_jual_tanam.status = 3 THEN "Proses Panen"
                                    WHEN m_jual_tanam.status = 4 THEN "Pelunasan"
                                    WHEN m_jual_tanam.status = 5 THEN "Selesai"
                                    WHEN m_jual_tanam.status = 6 THEN "Tolak"
                                    ELSE
                                        "Siap Jual"
                                END AS status_jual')
                            )
                ->leftJoin('m_user','m_user.uuid', 'm_data_tanam.uuid_user')
                ->leftJoin('m_nama_lembaga','m_nama_lembaga.id','m_user.id_lembaga')
                ->leftJoin('m_wilayah_lembaga','m_wilayah_lembaga.lembaga_id','m_nama_lembaga.id')
                ->leftJoin('m_informasi_tanam', 'm_informasi_tanam.kode_tanam','m_data_tanam.kode_tanam')
                ->leftJoin('m_lokasi_tanam', 'm_lokasi_tanam.kode_tanam', 'm_data_tanam.kode_tanam')
                ->leftJoin('m_jual_tanam','m_jual_tanam.kode_tanam', 'm_data_tanam.kode_tanam')
                ->leftJoin('provinces','provinces.id', 'm_wilayah_lembaga.prov_id')
                ->leftJoin('districts','districts.id', 'm_wilayah_lembaga.dis_id')
                ->leftJoin('subdistricts','subdistricts.id', 'm_wilayah_lembaga.subdis_id')
                ->leftJoin('cities','cities.id', 'm_wilayah_lembaga.city_id')
                ->where('m_data_tanam.kode_tanam',$request->kode_tanam)
                ->first();

        $response = array(
            "status" => true,
            "massage" => "detail data tanam",
            "data" => $data
        );
        
        return response()->json($response);


    }

   
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        //
    }

    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'kode_tanam' => 'required',
        ));

        if($validator->fails()){
            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        $data = DB::table('m_data_tanam')
                    ->where('kode_tanam', $request->kode_tanam)
                    ->delete();
                DB::table('m_informasi_tanam')
                    ->where('kode_tanam', $request->kode_tanam)
                    ->delete();
                DB::table('m_lokasi_tanam')
                    ->where('kode_tanam', $request->kode_tanam)
                    ->delete();
        if($data){
            $response = array(
                "status" => true,
                "massage" => "detail data tanam berhasil dihapus",
            );
            
            return response()->json($response);
        }else{
            $response = array(
                "status" => true,
                "massage" => "detail data tanam berhasil dihapus",
            );
            
            return response()->json($response);
        }
        
    }

    function jualtanam(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'uuid_user' => 'required|exists:m_user,uuid',
            'kode_tanam' => 'required|exists:m_data_tanam,kode_tanam|unique:m_jual_tanam,kode_tanam',
            'metode_panen' => 'required',
            'jadwal_panen' => 'required'
        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }



        $count_code = str_pad(DB::table('m_jual_tanam')->count(), 10, '0', STR_PAD_LEFT);
        $random_code = strtoupper(Str::random(4));
        $code_reg = 'HT-'.$count_code;

        DB::beginTransaction();
        
        try {

            $data = DB::table('m_jual_tanam')
                        ->insert([
                            'kode_jual' => $code_reg,
                            'uuid_user' => $request->uuid_user,
                            'kode_tanam' => $request->kode_tanam,
                            'metode_panen' => $request->metode_panen,
                            'jadwal_panen' => Carbon::parse($request->jadwal_panen)->format('Y-m-d'),
                            'status' => 0,
                            "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                        ]);

                    DB::table('m_data_tanam')
                        ->where('kode_tanam', $request->kode_tanam)
                        ->update(['status' => 1]);

                    DB::table('m_jual_tanam_note')
                        ->insert([
                            "kode_jual" => $code_reg,
                            "note" => $request->note,
                            "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                        ]);

                     DB::table('m_jual_tanam_harga')
                        ->insert([
                            "kode_jual" => $code_reg,
                            "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                        ]);

            $user = User::leftJoin('m_alamat_user', 'm_alamat_user.uuid_user', '=','m_user.uuid')
                            ->where('m_user.uuid',$request->uuid_user)
                            ->first();

            $lembaga = User::leftJoin('m_alamat_user', 'm_alamat_user.uuid_user', '=','m_user.uuid')
                            ->leftJoin('m_token_firebase', 'm_token_firebase.uuid_user','=','m_user.uuid' )
                            ->where('m_user.id_lembaga',$user->id_lembaga)
                            ->where('m_user.status',1)
                            ->first();

            $data_tanam = DB::table('m_jual_tanam')
                            ->select('m_data_tanam.varietas', 'm_data_tanam.komoditas','m_data_tanam.title')
                            ->leftJoin('m_data_tanam','m_data_tanam.kode_tanam','m_jual_tanam.kode_tanam')
                            ->where('m_jual_tanam.kode_jual', $code_reg)
                            ->first();

            $titel = $user->nama_depan." Mengajukan Penjualan ".$data_tanam->title." ".$data_tanam->komoditas." ".$data_tanam->varietas;
            $pesan = "Hai ".$lembaga->nama_depan.", cek status pengajuan sekarang";
            $type = 1;

            $notif = (new NotificationController)->StatusLaporan($type, $lembaga->token, $pesan, $titel, $code_reg);

            DB::table('user_notifikasi') 
                ->insert([
                    "uuid_user" => $lembaga->uuid,
                    "title" => $titel,
                    "body" => $pesan,
                    "status_read" => 0,
                    "kode_jual" => $code_reg,
                    "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                ]);

            DB::commit();

            $response = array(
                "status" => true,
                "massage" => "Penjualan berhasil diajukan",
                "data" => DB::table('m_jual_tanam')
                                ->where('kode_jual', $code_reg)
                                ->first(),
                "notif" => $notif, 
                "lembaga" =>  $lembaga
            );

            return response()->json($response);

        } catch (\Exception $e) {
            //throw $th;
            DB::rollback();

            $data = $e->getMessage();

            $response = array(
                "status" => false,
                "data" => $data
            );

            return response()->json($response);
        }

    }

    function updatestatusJual(Request $request) 
    {
        $validator = Validator::make($request->all(), array(
            'kode_jual' => 'required|exists:m_jual_tanam,kode_jual',
            'status' => 'required|integer|between:1,5',
            'note' => 'required'
        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        DB::beginTransaction(); 

        try {
            DB::table('m_jual_tanam')
                ->where('kode_jual', $request->kode_jual)
                ->update([
                    'status' => $request->status,
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);

            DB::table('m_jual_tanam_note')
                ->where('kode_jual', $request->kode_jual)
                ->update(['note' =>  $request->note]);

           

            $user = DB::table('m_jual_tanam')
                    ->select("m_user.*","m_token_firebase.token","m_jual_tanam.*", "m_jual_tanam.status")
                    ->leftJoin('m_user', 'm_user.uuid','m_jual_tanam.uuid_user')
                    ->leftJoin('m_token_firebase', 'm_token_firebase.uuid_user','=','m_user.uuid' )
                    ->where('m_jual_tanam.kode_jual', $request->kode_jual)
                    ->first();

            $titel = "Cek status Penjualan hasil Tani Kamu";
            $pesan = "Hai ".$user->nama_depan.", cek status pengajuan sekarang";
            $type = 1;

            $notif = (new NotificationController)->StatusLaporan($type, $user->token, $pesan, $titel, $request->kode_jual);

            DB::table('user_notifikasi') 
                ->insert([
                    "uuid_user" => $user->uuid,
                    "title" => $titel,
                    "body" => $pesan,
                    "status_read" => 0,
                    "kode_jual" => $request->kode_jual,
                    "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                ]);
                
            DB::commit();


            $response = array(
                "status" => true,
                "massage" => "Penjualan berhasil dikonfrimasi",
                "notif" => $notif, 
                "user" =>  $user
            );

            return response()->json($response);

        } catch (\Exception $e) {
            //throw $th;
            DB::rollback();

            $data = $e->getMessage();

            $response = array(
                "status" => false,
                "data" => $data
            );

            return response()->json($response);
        }
        
    }



    // prose barang jual

    function updatestatusTolak(Request $request) 
    {
        // $data_tanam = DB::table('m_jual_tanam')
        //                         ->select('m_data_tanam.varietas', 'm_data_tanam.komoditas','m_data_tanam.title')
        //                         ->leftJoin('m_data_tanam','m_data_tanam.kode_tanam','m_jual_tanam.kode_tanam')
        //                         ->where('m_jual_tanam.kode_jual', $request->kode_jual)
        //                         ->first();
        // $titel = "Pengajuan ".$data_tanam->title ." ".$data_tanam->varietas." sudah Diterima";

        $validator = Validator::make($request->all(), array(
            'kode_jual' => 'required|exists:m_jual_tanam,kode_jual',
            'note' => 'required'
        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        DB::beginTransaction(); 

        try {
            DB::table('m_jual_tanam')
                ->where('kode_jual', $request->kode_jual)
                ->update([
                    'status' => 6,
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);

            DB::table('m_jual_tanam_note')
                ->where('kode_jual', $request->kode_jual)
                ->update(['note' =>  $request->note]);


            $user = DB::table('m_jual_tanam')
                    ->select("m_user.*","m_token_firebase.token","m_jual_tanam.*", "m_jual_tanam.status")
                    ->leftJoin('m_user', 'm_user.uuid','m_jual_tanam.uuid_user')
                    ->leftJoin('m_token_firebase', 'm_token_firebase.uuid_user','=','m_user.uuid' )
                    ->where('m_jual_tanam.kode_jual', $request->kode_jual)
                    ->first();

            $data_tanam = DB::table('m_jual_tanam')
                            ->select('m_data_tanam.varietas', 'm_data_tanam.komoditas','m_data_tanam.title')
                            ->leftJoin('m_data_tanam','m_data_tanam.kode_tanam','m_jual_tanam.kode_tanam')
                            ->where('m_jual_tanam.kode_jual', $request->kode_jual)
                            ->first();
            // komoditas dan varitas nama tim
            $titel = "Pengajuan ".$data_tanam->title ." ".$data_tanam->varietas." Tidak Diterima";
            $pesan = "Hai ".$user->nama_depan.", cek status pengajuan sekarang";
            $type = 1;

            $notif = (new NotificationController)->StatusLaporan($type, $user->token, $pesan, $titel, $request->kode_jual);

            DB::table('user_notifikasi') 
                ->insert([
                    "uuid_user" => $user->uuid,
                    "title" => $titel,
                    "body" => $pesan,
                    "status_read" => 0,
                    "kode_jual" => $request->kode_jual,
                    "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                ]);
                
            DB::commit();


            $response = array(
                "status" => true,
                "massage" => "Penjualan berhasil dikonfrimasi",
                "notif" => $notif, 
                "user" =>  $user
            );

            return response()->json($response);

        } catch (\Exception $e) {
            //throw $th;
            DB::rollback();

            $data = $e->getMessage();

            $response = array(
                "status" => false,
                "data" => $data
            );

            return response()->json($response);
        }
        
    }

    function updatestatusDiterima(Request $request) 
    {
        // $data_tanam = DB::table('m_jual_tanam')
        //                         ->select('m_data_tanam.varietas', 'm_data_tanam.komoditas','m_data_tanam.title')
        //                         ->leftJoin('m_data_tanam','m_data_tanam.kode_tanam','m_jual_tanam.kode_tanam')
        //                         ->where('m_jual_tanam.kode_jual', $request->kode_jual)
        //                         ->first();
        // $titel = "Pengajuan ".$data_tanam->title ." ".$data_tanam->varietas." sudah Diterima";

        $validator = Validator::make($request->all(), array(
            'kode_jual' => 'required|exists:m_jual_tanam,kode_jual',
            'note' => 'required'
        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        DB::beginTransaction(); 

        try {
            DB::table('m_jual_tanam')
                ->where('kode_jual', $request->kode_jual)
                ->update([
                    'status' => 1,
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);

            DB::table('m_jual_tanam_note')
                ->where('kode_jual', $request->kode_jual)
                ->update(['note' =>  $request->note]);


            $user = DB::table('m_jual_tanam')
                    ->select("m_user.*","m_token_firebase.token","m_jual_tanam.*", "m_jual_tanam.status")
                    ->leftJoin('m_user', 'm_user.uuid','m_jual_tanam.uuid_user')
                    ->leftJoin('m_token_firebase', 'm_token_firebase.uuid_user','=','m_user.uuid' )
                    ->where('m_jual_tanam.kode_jual', $request->kode_jual)
                    ->first();

            $data_tanam = DB::table('m_jual_tanam')
                            ->select('m_data_tanam.varietas', 'm_data_tanam.komoditas','m_data_tanam.title')
                            ->leftJoin('m_data_tanam','m_data_tanam.kode_tanam','m_jual_tanam.kode_tanam')
                            ->where('m_jual_tanam.kode_jual', $request->kode_jual)
                            ->first();
            // komoditas dan varitas nama tim
            $titel = "Pengajuan ".$data_tanam->title ." ".$data_tanam->varietas." sudah Diterima";
            $pesan = "Hai ".$user->nama_depan.", cek status pengajuan sekarang";
            $type = 1;

            $notif = (new NotificationController)->StatusLaporan($type, $user->token, $pesan, $titel, $request->kode_jual);

            DB::table('user_notifikasi') 
                ->insert([
                    "uuid_user" => $user->uuid,
                    "title" => $titel,
                    "body" => $pesan,
                    "status_read" => 0,
                    "kode_jual" => $request->kode_jual,
                    "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                ]);
                
            DB::commit();


            $response = array(
                "status" => true,
                "massage" => "Penjualan berhasil dikonfrimasi",
                "notif" => $notif, 
                "user" =>  $user
            );

            return response()->json($response);

        } catch (\Exception $e) {
            //throw $th;
            DB::rollback();

            $data = $e->getMessage();

            $response = array(
                "status" => false,
                "data" => $data
            );

            return response()->json($response);
        }
        
    }

    function updatestatusSurvei(Request $request) 
    {
        $validator = Validator::make($request->all(), array(
            'kode_jual' => 'required|exists:m_jual_tanam,kode_jual',
            'lokasi' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'kualitas_barang' => 'required',
            'kuantitas_barang' => 'required',
            'base64_gambar_invoice' => 'required'
        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        DB::beginTransaction(); 

        try {

            DB::table('m_jual_tanam_survei')
                ->insert([
                    'kode_jual' => $request->kode_jual,
                    'lokasi' => $request->lokasi,
                    'latitude' => $request->latitude,
                    'longitude' => $request->longitude,
                    'kualitas_barang' => $request->kualitas_barang,
                    'kuantitas_barang' => $request->kuantitas_barang,
                    'file_gambar' => $this->shareFilePhoto($request->base64_gambar),
                    "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                ]);

                DB::table('m_jual_tanam')
                ->where('kode_jual', $request->kode_jual)
                ->update([
                    'status' => 2,
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);

            $user = DB::table('m_jual_tanam')
                    ->select("m_user.*","m_token_firebase.token","m_jual_tanam.*", "m_jual_tanam.status")
                    ->leftJoin('m_user', 'm_user.uuid','m_jual_tanam.uuid_user')
                    ->leftJoin('m_token_firebase', 'm_token_firebase.uuid_user','=','m_user.uuid' )
                    ->where('m_jual_tanam.kode_jual', $request->kode_jual)
                    ->first();

            $data_tanam = DB::table('m_jual_tanam')
                            ->select('m_data_tanam.varietas', 'm_data_tanam.komoditas','m_data_tanam.title')
                            ->leftJoin('m_data_tanam','m_data_tanam.kode_tanam','m_jual_tanam.kode_tanam')
                            ->where('m_jual_tanam.kode_jual', $request->kode_jual)
                            ->first();
            // komoditas dan varitas nama tim
            $titel = "Pengajuan ".$data_tanam->title ." ".$data_tanam->varietas." telah disurevei";

            $pesan = "Hai ".$user->nama_depan.", cek status pengajuan sekarang";
            $type = 1;

            $notif = (new NotificationController)->StatusLaporan($type, $user->token, $pesan, $titel, $request->kode_jual);

            DB::table('user_notifikasi') 
                ->insert([
                    "uuid_user" => $user->uuid,
                    "title" => $titel,
                    "body" => $pesan,
                    "status_read" => 0,
                    "kode_jual" => $request->kode_jual,
                    "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                ]);

            DB::commit();

            $response = array(
                "status" => true,
                "massage" => "survei berhasil ditambahkan",
                "notif" => $notif
            );

            return response()->json($response);

        } catch (\Exception $e) {
            //throw $th;
            DB::rollback();

            $data = $e->getMessage();

            $response = array(
                "status" => false,
                "data" => $data
            );

            return response()->json($response);
        }



        
    }

    function updatestatusBayar(Request $request) 
    {
        $validator = Validator::make($request->all(), array(
            'kode_jual' => 'required|exists:m_jual_tanam,kode_jual',
        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        DB::beginTransaction(); 

        try {
            DB::table('m_jual_tanam')
                ->where('kode_jual', $request->kode_jual)
                ->update([
                    'status' => 3,
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);
           

            $user = DB::table('m_jual_tanam')
                    ->select("m_user.*","m_token_firebase.token","m_jual_tanam.*", "m_jual_tanam.status")
                    ->leftJoin('m_user', 'm_user.uuid','m_jual_tanam.uuid_user')
                    ->leftJoin('m_token_firebase', 'm_token_firebase.uuid_user','=','m_user.uuid' )
                    ->where('m_jual_tanam.kode_jual', $request->kode_jual)
                    ->first();

            $data_tanam = DB::table('m_jual_tanam')
                            ->select('m_data_tanam.varietas', 'm_data_tanam.komoditas','m_data_tanam.title')
                            ->leftJoin('m_data_tanam','m_data_tanam.kode_tanam','m_jual_tanam.kode_tanam')
                            ->where('m_jual_tanam.kode_jual', $request->kode_jual)
                            ->first();
            // komoditas dan varitas nama tim
            $titel = "Pengajuan ".$data_tanam->title ." ".$data_tanam->varietas." Proses Pembayaran";

            $pesan = "Hai ".$user->nama_depan.", cek status pengajuan sekarang";
            $type = 1;

            $notif = (new NotificationController)->StatusLaporan($type, $user->token, $pesan, $titel, $request->kode_jual);

            DB::table('user_notifikasi') 
                ->insert([
                    "uuid_user" => $user->uuid,
                    "title" => $titel,
                    "body" => $pesan,
                    "status_read" => 0,
                    "kode_jual" => $request->kode_jual,
                    "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                ]);
                
            DB::commit();


            $response = array(
                "status" => true,
                "massage" => "Penjualan berhasil dikonfrimasi",
                "notif" => $notif, 
                "user" =>  $user
            );

            return response()->json($response);

        } catch (\Exception $e) {
            //throw $th;
            DB::rollback();

            $data = $e->getMessage();

            $response = array(
                "status" => false,
                "data" => $data
            );

            return response()->json($response);
        }
        
    }

    function updatestatusPelunasan(Request $request) 
    {
        $validator = Validator::make($request->all(), array(
            'kode_jual' => 'required|exists:m_jual_tanam,kode_jual',
            'note' => 'required',
            'harga_beli' => 'required',
            'uang_muka' => 'required'
        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        DB::beginTransaction(); 

        try {
            DB::table('m_jual_tanam')
                ->where('kode_jual', $request->kode_jual)
                ->update([
                    'status' => 4,
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);

            DB::table('m_jual_tanam_note')
                ->where('kode_jual', $request->kode_jual)
                ->update(['note' =>  $request->note]);

            DB::table('m_jual_tanam_harga')
                ->where('kode_jual', $request->kode_jual)
                ->update(['harga_beli' =>  $request->harga_beli, 
                          'uang_muka' =>  $request->uang_muka
                        ]);
            

            $user = DB::table('m_jual_tanam')
                    ->select("m_user.*","m_token_firebase.token")
                    ->leftJoin('m_user', 'm_user.uuid','m_jual_tanam.uuid_user')
                    ->leftJoin('m_token_firebase', 'm_token_firebase.uuid_user','=','m_user.uuid' )
                    ->where('m_jual_tanam.kode_jual', $request->kode_jual)
                    ->first();

            $data_tanam = DB::table('m_jual_tanam')
                            ->select('m_data_tanam.varietas', 'm_data_tanam.komoditas','m_data_tanam.title')
                            ->leftJoin('m_data_tanam','m_data_tanam.kode_tanam','m_jual_tanam.kode_tanam')
                            ->where('m_jual_tanam.kode_jual', $request->kode_jual)
                            ->first();
            // komoditas dan varitas nama tim
            $titel = "Pengajuan ".$data_tanam->title ." ".$data_tanam->varietas." Proses Pelunasan";
            $pesan = "Hai ".$user->nama_depan.", cek status pengajuan sekarang";
            $type = 1;

            $notif = (new NotificationController)->StatusLaporan($type, $user->token, $pesan, $titel, $request->kode_jual);

            DB::table('user_notifikasi') 
                ->insert([
                    "uuid_user" => $user->uuid,
                    "title" => $titel,
                    "body" => $pesan,
                    "status_read" => 0,
                    "kode_jual" => $request->kode_jual,
                    "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                ]);

            DB::commit();


            $response = array(
                "status" => true,
                "massage" => "Penjualan berhasil dikonfrimasi",
                "notif" => $notif, 
                "user" =>  $user
            );

            return response()->json($response);

        } catch (\Exception $e) {
            //throw $th;
            DB::rollback();

            $data = $e->getMessage();

            $response = array(
                "status" => false,
                "data" => $data
            );

            return response()->json($response);
        }
        
    }

    function updatestatusSelesai(Request $request) 
    {
        $validator = Validator::make($request->all(), array(
            'kode_jual' => 'required|exists:m_jual_tanam,kode_jual',
            'note' => 'required',
            'hasil_panen' => 'required',
            'sisa_pelunasan' => 'required'
        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        DB::beginTransaction(); 

        try {
            DB::table('m_jual_tanam')
                ->where('kode_jual', $request->kode_jual)
                ->update([
                    'status' => 5,
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);

            DB::table('m_jual_tanam_note')
                ->where('kode_jual', $request->kode_jual)
                ->update(['note' =>  $request->note]);

            DB::table('m_jual_tanam_harga')
                ->where('kode_jual', $request->kode_jual)
                ->update(['hasil_panen' =>  $request->hasil_panen]);
            

            $user = DB::table('m_jual_tanam')
                    ->select("m_user.*","m_token_firebase.token")
                    ->leftJoin('m_user', 'm_user.uuid','m_jual_tanam.uuid_user')
                    ->leftJoin('m_token_firebase', 'm_token_firebase.uuid_user','=','m_user.uuid' )
                    ->where('m_jual_tanam.kode_jual', $request->kode_jual)
                    ->first();
            $data_tanam = DB::table('m_jual_tanam')
                            ->select('m_data_tanam.varietas', 'm_data_tanam.komoditas','m_data_tanam.title')
                            ->leftJoin('m_data_tanam','m_data_tanam.kode_tanam','m_jual_tanam.kode_tanam')
                            ->where('m_jual_tanam.kode_jual', $request->kode_jual)
                            ->first();
            // komoditas dan varitas nama tim
            $titel = "Pengajuan ".$data_tanam->title ." ".$data_tanam->varietas." Selesai diproses";
            $pesan = "Hai ".$user->nama_depan.", cek status pengajuan sekarang";
            $type = 1;

            $notif = (new NotificationController)->StatusLaporan($type, $user->token, $pesan, $titel, $request->kode_jual);

            DB::table('user_notifikasi') 
                ->insert([
                    "uuid_user" => $user->uuid,
                    "title" => $titel,
                    "body" => $pesan,
                    "status_read" => 0,
                    "kode_jual" => $request->kode_jual,
                    "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                ]);

            DB::commit();


            $response = array(
                "status" => true,
                "massage" => "Penjualan berhasil dikonfrimasi",
                "notif" => $notif, 
                "user" =>  $user
            );

            return response()->json($response);

        } catch (\Exception $e) {
            //throw $th;
            DB::rollback();

            $data = $e->getMessage();

            $response = array(
                "status" => false,
                "data" => $data
            );

            return response()->json($response);
        }
        
    }

// -----------------------



    function listjualtanam(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'uuid_user' => 'required|exists:m_user,uuid',
        ));

        if($validator->fails()){
            
            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }
        
        try {

            $data = DB::table('m_jual_tanam')
                      ->select('m_jual_tanam.*',
                                \DB::raw('CASE 
                                            WHEN m_jual_tanam.status = 0 THEN "Menunggu Konfirmasi"
                                            WHEN m_jual_tanam.status = 1 THEN "Pesanan diterima"
                                            WHEN m_jual_tanam.status = 2 THEN "Sedang di Survey"
                                            WHEN m_jual_tanam.status = 3 THEN "Proses Panen"
                                            WHEN m_jual_tanam.status = 4 THEN "Pelunasan"
                                            WHEN m_jual_tanam.status = 5 THEN "Selesai"
                                            WHEN m_jual_tanam.status = 6 THEN "Tolak"
                                            ELSE
                                                "Siap Jual"
                                          END AS status_jual'),
                                'm_data_tanam.*',
                                'm_nama_lembaga.*',
                                'm_jual_tanam.created_at')
                      ->leftJoin('m_user','m_user.uuid', 'm_jual_tanam.uuid_user')
                      ->leftJoin('m_nama_lembaga','m_nama_lembaga.id','m_user.id_lembaga')
                      ->leftJoin('m_data_tanam', 'm_data_tanam.kode_tanam','m_jual_tanam.kode_tanam' )
                      ->where('m_jual_tanam.uuid_user', $request->uuid_user)
                      ->orderBy('m_jual_tanam.id','desc')
                      ->get();         

            $response = array(
                "status" => true,
                "massage" => "Penjualan berhasil diajukan",
                "data" => $data
            );

            return response()->json($response);

        } catch (\Exception $e) {
            //throw $th;
            DB::rollback();

            $data = $e->getMessage();

            $response = array(
                "status" => false,
                "data" => $data
            );

            return response()->json($response);
        }

    }

    function detailjualtanam(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'kode_jual' => 'required|exists:m_jual_tanam,kode_jual',
        ));

        if($validator->fails()){
            
            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }
        
        try {

            $data = DB::table('m_jual_tanam')
                      ->select('m_jual_tanam.kode_jual', 
                                'm_jual_tanam.metode_panen',
                                'm_jual_tanam.jadwal_panen',
                                'm_jual_tanam.created_at',
                                'm_user.nama_depan',
                                'm_user.nama_belakang',
                                'm_user.nomor_hp',
                                'm_user.email',
                                'm_jual_tanam_harga.uang_muka',
                                'm_jual_tanam_harga.harga_beli',
                                'm_jual_tanam.status as kode_status_jual',
                                \DB::raw('CASE 
                                            WHEN m_jual_tanam.status = 0 THEN "Menunggu Konfirmasi"
                                            WHEN m_jual_tanam.status = 1 THEN "Pesanan diterima"
                                            WHEN m_jual_tanam.status = 2 THEN "Sedang di Survey"
                                            WHEN m_jual_tanam.status = 3 THEN "Proses Panen"
                                            WHEN m_jual_tanam.status = 4 THEN "Pelunasan"
                                            WHEN m_jual_tanam.status = 5 THEN "Selesai"
                                            WHEN m_jual_tanam.status = 6 THEN "Tolak"
                                            ELSE
                                                "Siap Jual"
                                          END AS status_jual'),
                                'm_data_tanam.title',
                                'm_data_tanam.varietas',
                                'm_informasi_tanam.*',
                                'm_nama_lembaga.*',
                                'm_jual_tanam_note.note',
                                'm_jual_tanam.created_at')
                      ->leftJoin('m_user','m_user.uuid', 'm_jual_tanam.uuid_user')
                      ->leftJoin('m_nama_lembaga','m_nama_lembaga.id','m_user.id_lembaga')
                      ->leftJoin('m_data_tanam', 'm_data_tanam.kode_tanam','m_jual_tanam.kode_tanam')
                      ->leftJoin('m_informasi_tanam','m_informasi_tanam.kode_tanam','m_jual_tanam.kode_tanam')
                      ->leftJoin('m_jual_tanam_note','m_jual_tanam_note.kode_jual', 'm_jual_tanam.kode_jual')
                      ->leftJoin('m_jual_tanam_harga','m_jual_tanam_harga.kode_jual','m_jual_tanam.kode_jual')
                      ->where('m_jual_tanam.kode_jual', $request->kode_jual)
                      ->first();         

            $response = array(
                "status" => true,
                "massage" => "detail",
                "data" => $data
            );

            return response()->json($response);

        } catch (\Exception $e) {
            //throw $th;
            DB::rollback();

            $data = $e->getMessage();

            $response = array(
                "status" => false,
                "data" => $data
            );

            return response()->json($response);
        }

    }

    function notifikasilist(Request $request) 
    {
        $validator = Validator::make($request->all(), array(
            'uuid_user' => 'required|exists:m_user,uuid',
        ));

        if($validator->fails()){
            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        $data = DB::table('user_notifikasi') 
                    ->select('user_notifikasi.*', 
                             'user_notifikasi.id as id_notifikasi',
                             \DB::raw('CASE 
                                            WHEN m_jual_tanam.status = 0 THEN "Menunggu Konfirmasi"
                                            WHEN m_jual_tanam.status = 1 THEN "Pesanan diterima"
                                            WHEN m_jual_tanam.status = 2 THEN "Sedang di Survey"
                                            WHEN m_jual_tanam.status = 3 THEN "Proses Panen"
                                            WHEN m_jual_tanam.status = 4 THEN "Pelunasan"
                                            WHEN m_jual_tanam.status = 5 THEN "Selesai"
                                            WHEN m_jual_tanam.status = 6 THEN "Tolak"
                                            ELSE
                                                "Siap Jual"
                                          END AS status_jual'))
                    ->leftJoin('m_jual_tanam', 'm_jual_tanam.kode_jual', 'user_notifikasi.kode_jual')
                    ->where('user_notifikasi.uuid_user', $request->uuid_user)
                    ->orderBy('id', 'desc')
                    ->get();

        if (count($data)>0) {
            $response = array(
                "status" => true,
                "massage" => "list notifikasi",
                "data" => $data
            );
        }else{
            $response = array(
                "status" => false,
                "massage" => "list notifikasi tidak ada",
                "data" => []
            );
        }

        return response()->json($response);
    }

    function updateNotifikasi(Request $request) 
    {
        $validator = Validator::make($request->all(), array(
            'id_notifikasi' => 'required|exists:user_notifikasi,id',
        ));

        if($validator->fails()){
            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        DB::table('user_notifikasi') 
                ->where('id', $request->id_notifikasi)
                ->update([
                    "status_read" => 1,
                ]);

        $response = array(
                "status" => true,
                "massage" => "notifikasi berhasi dibaca",
            );

        return response()->json($response);
    }


    function shareFilePhoto($foto)
    {
        $random = Str::random(15);
        $dir_name = '/storage/gambar';
        $dir = public_path($dir_name);
        $filename    = $dir . '/' . $random.'.jpg';
        
        if(!File::isDirectory($dir))
        {

            File::makeDirectory($dir, 0777, true, true);     

        }
        
        if (isset($foto)) {
            $image_name = $filename;
            // base64 encoded utf-8 string
            $binary = base64_decode($foto);
            // binary, utf-8 bytes
            $success = file_put_contents($image_name, $binary);
            if ($success==FALSE) {
               return  FALSE;
            }else{
                return $dir_name.'/'.$random.'.jpg' ;
            }
        }else{
            return FALSE;
        }
    }


    function totalDataTanam(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'uuid_user' => 'required|exists:m_user,uuid|exists:m_data_tanam,uuid_user',
        ));

        if($validator->fails()){
            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        $data =  DB::table('m_data_tanam')
                    ->select('m_data_tanam.kode_tanam', 'm_data_tanam.title')
                    ->where('m_data_tanam.uuid_user', $request->uuid_user)
                    ->where('m_data_tanam.status', 0)
                    ->get();

        $data_jual = DB::table('m_jual_tanam')
                        ->select('m_data_tanam.kode_tanam', 'm_data_tanam.title', )
                        ->where('m_data_tanam.uuid_user', $request->uuid_user)
                        ->leftJoin('m_data_tanam', 'm_data_tanam.kode_tanam','m_jual_tanam.kode_tanam' )
                        ->whereNotIn('m_jual_tanam.status', [5,6])
                        ->whereNotNull('m_jual_tanam.status')
                        ->get();

        $arrayData = (object) [
            "total_data_tanam" => count($data),
            "total_data_jual" => count($data_jual)
        ];

        $response = array(
            "status" => true,
            "massage" => "Total",
            "data" => $arrayData
        );
        
        
        
        return response()->json($response);
    }

}
