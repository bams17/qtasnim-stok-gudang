<?php

namespace App\Http\Controllers\Api\Mitra;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Str;
use File;
use DB;
use Mail;
use Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Http\Controllers\Admin\NotificationController;

class mitraController extends Controller
{
   
    public function index()
    {
        //
    }

    
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        //
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }

    function listpengajuan(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'uuid_user' => 'required|exists:m_user,uuid',
        ));

        if($validator->fails()){
            
            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }
        
        try {

            $user = User::leftJoin('m_alamat_user', 'm_alamat_user.uuid_user', '=','m_user.uuid')
                        ->where('m_user.uuid',$request->uuid_user)
                        ->first();
                        
            if($user->status != 1){
                $response = array(
                    "status"   => false,
                    "message"   => "anda tidak memiliki akses ini",
                );
                return response()->json($response);
            }

            $data = DB::table('m_jual_tanam')
                      ->select('m_jual_tanam.*', 
                                'm_user.nama_depan',
                                'm_user.nama_belakang',
                                'm_user.nomor_hp',
                                \DB::raw('CASE 
                                                WHEN m_jual_tanam.status = 0 THEN "Menunggu Konfirmasi"
                                                WHEN m_jual_tanam.status = 1 THEN "Pesanan diterima"
                                                WHEN m_jual_tanam.status = 2 THEN "Sedang di Survey"
                                                WHEN m_jual_tanam.status = 3 THEN "Proses Panen"
                                                WHEN m_jual_tanam.status = 4 THEN "Pelunasan"
                                                WHEN m_jual_tanam.status = 5 THEN "Selesai"
                                                WHEN m_jual_tanam.status = 6 THEN "Tolak"
                                                ELSE
                                                    "Siap Jual"
                                            END AS status_jual'),
                                'm_data_tanam.*',
                                'm_nama_lembaga.*',
                                'provinces.prov_name',
                                'm_lokasi_tanam.alamat',
                                'cities.city_name',
                                'm_jual_tanam.created_at')
                      ->leftJoin('m_user','m_user.uuid', 'm_jual_tanam.uuid_user')
                      ->leftJoin('m_nama_lembaga','m_nama_lembaga.id','m_user.id_lembaga')
                      ->leftJoin('m_data_tanam', 'm_data_tanam.kode_tanam','m_jual_tanam.kode_tanam' )
                      ->leftJoin('m_lokasi_tanam', 'm_lokasi_tanam.kode_tanam', 'm_data_tanam.kode_tanam')
                      ->leftJoin('provinces','provinces.id', 'm_lokasi_tanam.prov_id')
                      ->leftJoin('cities','cities.id', 'm_lokasi_tanam.city_id')
                      ->where('m_nama_lembaga.id', $user->id_lembaga)
                      ->orderBy('m_jual_tanam.id', 'desc')
                      ->get();         

           if(count($data)>0){
                $response = array(
                    "status" => true,
                    "massage" => "list pengajuan penjualan",
                    "data" => $data
                );
           }else {
                $response = array(
                    "status" => false,
                    "massage" => "tidak ada list pengajuan",
                    "data" => $data
                );
           }
            

            return response()->json($response);

        } catch (\Exception $e) {
            //throw $th;
            DB::rollback();

            $data = $e->getMessage();

            $response = array(
                "status" => false,
                "data" => $data
            );

            return response()->json($response);
        }

    }


    function totalPengajuan(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'uuid_user' => 'required|exists:m_user,uuid',
        ));

        if($validator->fails()){
            
            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }
        
        try {

            $user = User::leftJoin('m_alamat_user', 'm_alamat_user.uuid_user', '=','m_user.uuid')
                        ->where('m_user.uuid',$request->uuid_user)
                        ->first();
                        
            if($user->status != 1){
                $response = array(
                    "status"   => false,
                    "message"   => "anda tidak memiliki akses ini",
                );
                return response()->json($response);
            }

            $data = DB::table('m_jual_tanam')
                      ->select('m_jual_tanam.*', 
                                'm_user.nama_depan',
                                'm_user.nama_belakang',
                                'm_user.nomor_hp',
                                \DB::raw('CASE 
                                                WHEN m_jual_tanam.status = 0 THEN "Menunggu Konfirmasi"
                                                WHEN m_jual_tanam.status = 1 THEN "Pesanan diterima"
                                                WHEN m_jual_tanam.status = 2 THEN "Sedang di Survey"
                                                WHEN m_jual_tanam.status = 3 THEN "Proses Panen"
                                                WHEN m_jual_tanam.status = 4 THEN "Pelunasan"
                                                WHEN m_jual_tanam.status = 5 THEN "Selesai"
                                                WHEN m_jual_tanam.status = 6 THEN "Tolak"
                                                ELSE
                                                    "Siap Jual"
                                            END AS status_jual'),
                                'm_data_tanam.*',
                                'm_nama_lembaga.*',
                                'provinces.prov_name',
                                'm_lokasi_tanam.alamat',
                                'cities.city_name',
                                'm_jual_tanam.created_at')
                      ->leftJoin('m_user','m_user.uuid', 'm_jual_tanam.uuid_user')
                      ->leftJoin('m_nama_lembaga','m_nama_lembaga.id','m_user.id_lembaga')
                      ->leftJoin('m_data_tanam', 'm_data_tanam.kode_tanam','m_jual_tanam.kode_tanam' )
                      ->leftJoin('m_lokasi_tanam', 'm_lokasi_tanam.kode_tanam', 'm_data_tanam.kode_tanam')
                      ->leftJoin('provinces','provinces.id', 'm_lokasi_tanam.prov_id')
                      ->leftJoin('cities','cities.id', 'm_lokasi_tanam.city_id')
                      ->where('m_nama_lembaga.id', $user->id_lembaga)
                      ->whereNotIn('m_jual_tanam.status', [6,5])
                      ->whereNotNull('m_jual_tanam.status')
                      ->orderBy('m_jual_tanam.id', 'desc')
                      ->get();  

            $response = array(
                "status" => true,
                "massage" => "total pengajuan",
                "data" => (object)[
                    "total_pengajuan" => count($data)
                ]
            );
            

            return response()->json($response);

        } catch (\Exception $e) {
            //throw $th;
            DB::rollback();

            $data = $e->getMessage();

            $response = array(
                "status" => false,
                "data" => $data
            );

            return response()->json($response);
        }

    }
}
