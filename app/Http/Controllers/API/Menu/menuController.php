<?php

namespace App\Http\Controllers\API\Menu;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Str;
use File;
use DB;
use Mail;
use Carbon\Carbon;

class menuController extends Controller
{
    //
    public function listmenuheader(Type $var = null)
    {


        try {
            $data_menu = DB::table('m_menu')->get();

            $data_menu_result = [];

            for ($i=0; $i < count($data_menu); $i++) { 
                
                $data_submenu = DB::table('m_submenu')
                                        ->where('menu_id', $data_menu[$i]->id)
                                        ->get();
                $data = array(
                    "nama_menu" => $data_menu[$i]->menu,
                    "url" => $data_menu[$i]->url,
                    "status_sub_menu" => $data_menu[$i]->type == 2 ? true : false,
                    "sub_menu" => $data_submenu
                );

                array_push($data_menu_result, $data);
            }

            $response = array(
                "status"   => true,
                "message"   => "List menu",
                "data"      => $data_menu_result
            );
            
        return response()->json($response);

        } catch (\Exception $e) {

            $response = array(
                "status"   => false,
                "message"   => "Erro",
                "data"      => $e
            );

            return response()->json($response);
        }

        # code...
    }

}
