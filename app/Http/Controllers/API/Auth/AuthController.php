<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Str;
use File;
use DB;
use Mail;
use Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Http\Controllers\Admin\NotificationController;
use App\Mail\RegistrasiMail;
use DNS2D;


class AuthController extends Controller
{
    // public function __construct()
    // {
    //   $this->middleware('jwt.verify')->except(['login','registrasi','profile']);
    // }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'email' => 'required|email',
        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        
        $user = User::leftJoin('m_alamat_user', 'm_alamat_user.uuid_user', '=','m_user.uuid')
                    ->leftJoin('m_nama_lembaga','m_nama_lembaga.id','m_user.id_lembaga')
                    ->where('email',$request->email)
                    ->first();

        if (!$user) {  
            return response()->json(['status'=>false, 'message' => 'Login gagal, silahkan cek email']);
        }

        if($request->password != null) {    
            if (!Hash::check($request->password, $user->password)) {
                 return response()->json(['status'=>false, 'message' => 'Login gagal, silahkan cek email/password anda ']);
            }
        }
        
        if (!$token = auth()->login($user)) {
            
            return response()->json(['error' => 'Unauthorized']);

        }


        return response()->json([
                                'status'=>true,
                                'message'=>'success',
                                'data'=> array(
                                    'token'=>$this->respondWithToken($token),
                                    'profile' => $user)
                                ]);

    }

    public function registrasi(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'nama_depan' => 'required',
            'nomor_hp' => 'required',
            'email'=>'required|unique:m_user,email',
            'password'=>'required',
            'link_gambar'=> 'required',
            'id_lembaga' => 'required',
            'provinsi' => 'required',
            'kota'=> 'required',
            'kec'=> 'required',
            'kode_pos'=> 'required',
            'alamat_lengkap' => 'required',
            'nama_bank' => 'required',
            'nama_pemilik_rekening' => 'required',
            'no_rekening' => 'required'

        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        $count_code = str_pad(DB::table('m_user')->count(), 4, '0', STR_PAD_LEFT);
        $random_code = strtoupper(Str::random(4));
        $code_reg = 'HV-'.$random_code."-".$count_code;

        $insert = array(
            'nama_depan' => $request->nama_depan,
            'nama_belakang' => $request->nama_belakang,
            'nomor_hp' => $request->nomor_hp,
            'email'=>$request->email,
            'password'=> Hash::make($request->password),
            'link_gambar'=> $this->shareFilePhoto($request->link_gambar),
            'uuid' => $code_reg,
            'id_lembaga' => $request->id_lembaga,
            "created_at" => Carbon::now()->format('Y-m-d H:i:s')
        );

        $insertWilayah = array(
            'uuid_user' => $code_reg,
            'prov_id' => $request->provinsi,
            'city_id' => $request->kota,
            'dis_id'=> $request->kec,
            'subdis_id' => $request->desa,
            'kode_pos' => $request->kode_pos,
            'alamat_lengkap' => $request->alamat_lengkap,
            "created_at" => Carbon::now()->format('Y-m-d H:i:s')
        );

        $insertRek = array(
            'uuid_user' => $code_reg,
            'nama_bank' => $request->nama_bank,
            'nama_pemilik_rekening' => $request->nama_pemilik_rekening,
            'no_rekening' => $request->no_rekening,
            "created_at" => Carbon::now()->format('Y-m-d H:i:s')
        );

        DB::beginTransaction();

        try {

            DB::table('m_user')
                ->insert($insert);
        
            DB::table('m_alamat_user')
                ->insert($insertWilayah);

            DB::table('m_rekening')
                ->insert($insertRek);

            DB::commit();

            $data = "Registrasi berhasil";

            $response = array(
                "status" => true,
                "data" => $insert
            );


        } catch (\Exception $e) {

            DB::rollback();

            $data = $e->getMessage();

            $response = array(
                "status" => false,
                "data" => $data
            );

        }

        try {

            // $berita = DB::table('m_seo')
            //             ->inRandomOrder()
            //             ->value('slug');
            
            // $pesan = "Assalamu'alaikum wr wb.\\r\\n\\r\\nBapak/Ibu ". $request->nama ." yang saya hormati \\r\\n\\r\\nSemoga Allah SWT senantiasa memberikan rahmat serta lindungannya kepada kita semua\\r\\n\\r\\nSaya ".env('NAMA_CALEG')." mengucapkan selamat bergabung di Tim BARA (Baraya Teh Tiara) Kepada seluruh LASKAR 03, saya sampaikan selamat berjuang ! masa depan adalah kita.\\r\\n\\r\\n'Sebaik-baik manusia adalah manusia yang paling bermanfaat bagi manusia yang lainnya' itulah semboyan perjuangan kita.\\r\\n\\r\\nAtas segala perhatian dan atensinya saya Tiara Putri Julizar mengucapkan banyak terima kasih, semoga Allah SWT membalas segala kebaikan didalam perjuangan ini dengan pahala kebaikan yang berlipat ganda. \\r\\n\\r\\nKita semua yakin bahwa kebaikan hanya akan dibalas dengan kebaikan. Mari kita awali perjuangan ini dengan niat baik.\\r\\n\\r\\n Terima kasih \\r\\n\\r\\n Wassalamu'alaikum wr wb \\r\\n\\r\\n Salam hormat,\\r\\n\\r\\n Hj. Tiara Putri Julizar, SH \\r\\n\\r\\n Kunjungi untuk mengenal lebih dekat 👉🏻 ".env('WEB_PROFILE')."/"."info/".$berita;

            // (new NotificationController)->SendwhatsApp($request->nomor_hp, $pesan);

            
        } catch (\Throwable $th) {
            //throw $th;
            dd($th);
        }
            
        

        return response()->json($response);


    }

    function lupaPassword(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'email'=>'required|exists:m_user,email',
        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        $user = User::leftJoin('m_alamat_user', 'm_alamat_user.uuid_user', '=','m_user.uuid')
                    ->where('email',$request->email)
                    ->first();
     

        $code_reg = (string) rand(1000,9999);

        $storage = public_path('/storage/rekrutmen/barcode');

        try {
            $notice = 30;
            DB::table('m_verikasi_kode')
                 ->insert([
                    "uuid_user" => $user->uuid,
                    "code_verifikasi" => $code_reg,
                    "status_verifkasi" => 0,
                    "expired_date" => Carbon::now()->addMinutes($notice)->format('Y-m-d H:i:s'),
                    "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                 ]);

            if(!File::isDirectory($storage)){

            File::makeDirectory($storage, 0777, true, true); 
            file_put_contents($storage.'/'.$code_reg.'.png', base64_decode(DNS2D::getBarcodePNG($code_reg, 'QRCODE')));

            }else{

                file_put_contents($storage.'/'.$code_reg.'.png', base64_decode(DNS2D::getBarcodePNG($code_reg, 'QRCODE')));
        
            }

            $data = (new RegistrasiMail)->kirimemailregister($user->nama_depan, $request->email, $code_reg,'/storage/rekrutmen/barcode/'.$code_reg.'.png');

        
            $response = array(
                "status"   => true,
                "message"   => "Silahkan cek kotak masuk/spam email untuk kode verifikasi",
            );

            return response()->json($response);

        } catch (\Exception $e) {

            $data = $e->getMessage();
            $response = array(
                "status" => false,
                "massage" => "error, tidak bisa melakukan proses",
                "data" => $data
            );

            return response()->json($response);
        }

        
    }

    function cekverfikasicode(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'code_verifikasi'=>'required|exists:m_verikasi_kode,code_verifikasi',
        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        $data = DB::table('m_verikasi_kode')
                    ->select('m_verikasi_kode.*', 'm_user.email')
                    ->leftJoin('m_user', 'm_user.uuid', 'm_verikasi_kode.uuid_user')
                    ->where('code_verifikasi', $request->code_verifikasi)
                    ->where('status_verifkasi', 0)
                    ->first();
        if(!$data){
            $response = array(
                "status" => false,
                "message" => "kode verifikasi sudah digunakan silahkan untuk meminta kode verifikasi baru"
            );
            return response()->json($response);
        }

        $now = Carbon::now();
        $start_date = Carbon::now()->format('Y-m-d H:i:s');
        $end_date = Carbon::parse($data->expired_date)->format('Y-m-d H:i:s');

        try {
            if($now->between($start_date,$end_date)){
                DB::table('m_verikasi_kode')
                    ->where('code_verifikasi', $request->code_verifikasi)
                    ->update(['status_verifkasi' => 1]);

                $response = array(
                    "status"   => true,
                    "message"   => "Berhasil melakukan verifikasi kode, silahkan untuk merubah password baru",
                    "data"      => $data
                );

                return response()->json($response);
            }else{
                $response = array(
                    "status"   => false,
                    "message"   => "Kode verifikasi sudah expired/kadaluarsa"
                );

                return response()->json($response);
            }
           
                
        } catch (\Exception $e) {
            $data = $e->getMessage();
            $response = array(
                "status" => false,
                "massage" => "error, tidak bisa melakukan proses",
                "data" => $data
            );

            return response()->json($response);
        }

        



    }

    function rubahpassword(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'email'=>'required|exists:m_user,email',
            'code_verifikasi'=>'required|exists:m_verikasi_kode,code_verifikasi',
            'password'=>'required',
        ));

        if($validator->fails()){
            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        try {
            $user = User::leftJoin('m_verikasi_kode','m_verikasi_kode.uuid_user', 'm_user.uuid')
                     ->where('m_user.email',$request->email)
                     ->where('m_verikasi_kode.code_verifikasi', $request->code_verifikasi)
                     ->where('m_verikasi_kode.status_verifkasi', 1)
                     ->update(['password'=> Hash::make($request->password)]);
            if($user){
                
                DB::table('m_verikasi_kode')
                    ->where('code_verifikasi', $request->code_verifikasi)
                    ->update(['status_verifkasi' => 2]);

                $response = array(
                    "status"   => true,
                    "message"   => "password berhasil dirubah silahkan untuk login/masuk kembali",
                );
    
                return response()->json($response);
            }else{
                $response = array(
                    "status"   => false,
                    "message"   => "password gagal dirubah silahkan melakukan melakukan verifikasi kode kembali",
                );
    
                return response()->json($response);
            }

        } catch (\Exception $e) {

            $data = $e->getMessage();
            $response = array(
                "status" => false,
                "massage" => "error, tidak bisa melakukan proses",
                "data" => $data
            );

            return response()->json($response);
        }

        






        
    }

    

   public function userProfile(Request $request)
   {
        $validator = Validator::make($request->all(), array(
            'email' => 'required|email',
        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        $user = User::select('m_user.*', 'm_alamat_user.*','m_nama_lembaga.*', 'm_nama_lembaga.nama_tim as nama_lembaga')
                    ->leftJoin('m_alamat_user', 'm_alamat_user.uuid_user', '=','m_user.uuid')
                     ->leftJoin('m_nama_lembaga','m_nama_lembaga.id','m_user.id_lembaga')
                     ->where('m_user.email',$request->email)
                     ->first();

        if (!$user) {  
            return response()->json(['status'=>false, 'message' => 'Login gagal, silahkan cek email']);
        }

        return response()->json([
            'status'=>true,
            'message'=>'success',
            'data'=> $user]);

   }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function updateToken(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'uuid_user' => 'required|exists:m_user,uuid',
            'token' => 'required',
        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        try {
            $user = DB::table('m_token_firebase')
                    ->where('uuid_user', $request->uuid_user)
                    ->first();
            if($user){

                DB::table('m_token_firebase')
                    ->where("uuid_user", $request->uuid_user)
                    ->update([
                            "token" => $request->token,
                            "updated_at" => Carbon::now()->format('Y-m-d H:i:s')
                    ]);
            }else{
                DB::table('m_token_firebase')
                        ->insert([
                            "uuid_user" => $request->uuid_user, 
                            "token" => $request->token,
                            "created_at" => Carbon::now()->format('Y-m-d H:i:s')
                        ]);

            }

            $response = array(
                "status"   => true,
                "message"   => "update token berhasil",
            );

            return response()->json($response);
        } catch (\Exaptions $th) {
            $response = array(
                "status"    => false,
                "message"   => "Tidak bisa melakukan proses data",
                "error"     => $th->getMassage()
            );
            return response()->json($response);
        }


    }

    function provinsi()
    {
        $data = DB::table('provinces')
                    ->get();
        return response()->json(['status'=>true, 'message' => 'list provinsi', 'data' => $data]);
        
    }

    function kota(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'prov_id' => 'required',
        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        $data = DB::table('cities')
                    ->where('prov_id', $request->prov_id)
                    ->get();

        return response()->json(['status'=>true, 'message' => 'list kota', 'data' => $data]);
        
    }

    function kecamatan(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'city_id' => 'required',
        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        $data = DB::table('districts')
                    ->where('city_id', $request->city_id)
                    ->get();

        return response()->json(['status'=>true, 'message' => 'list provinsi', 'data' => $data]);
        
    }

    function desa(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'dis_id' => 'required',
        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        $data = DB::table('subdistricts')
                    ->where('dis_id', $request->dis_id)
                    ->get();

        return response()->json(['status'=>true, 'message' => 'list desa', 'data' => $data]);
        
    }

    function kodepos(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'subdis_id' => 'required',
        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        $data = DB::table('postalcode')
                    ->select('postal_code')
                    ->where('subdis_id', $request->subdis_id)
                    ->first();

        return response()->json(['status'=>true, 'message' => 'list kode pos', 'data' => $data]);
        
    }

    function shareFilePhoto($foto)
    {
        $random = Str::random(15);
        $dir_name = '/storage/gambar';
        $dir = public_path($dir_name);
        $filename    = $dir . '/' . $random.'.jpg';
        
        if(!File::isDirectory($dir))
        {

            File::makeDirectory($dir, 0777, true, true);     

        }
        
        if (isset($foto)) {
            $image_name = $filename;
            // base64 encoded utf-8 string
            $binary = base64_decode($foto);
            // binary, utf-8 bytes
            $success = file_put_contents($image_name, $binary);
            if ($success==FALSE) {
               return  FALSE;
            }else{
                return $dir_name.'/'.$random.'.jpg' ;
            }
        }else{
            return FALSE;
        }
    }
    
}
