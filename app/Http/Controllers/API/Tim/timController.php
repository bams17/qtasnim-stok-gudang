<?php

namespace App\Http\Controllers\API\Tim;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Str;
use File;
use DB;
use Mail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class timController extends Controller
{
    //
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'subdis_id' => 'required',
            'dis_id' => 'required',
            'city_id' => 'required',
        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        try {
            $data =  DB::table('m_nama_lembaga')
                            ->select('m_nama_lembaga.*',
                                    'm_wilayah_lembaga.prov_id',
                                    'm_wilayah_lembaga.city_id',
                                    'm_wilayah_lembaga.dis_id',
                                    'm_wilayah_lembaga.subdis_id',
                                    'provinces.prov_name',
                                    'cities.city_name',
                                    'districts.dis_name',
                                    'subdistricts.subdis_name')
                            ->leftJoin('m_wilayah_lembaga', 'm_wilayah_lembaga.lembaga_id', 'm_nama_lembaga.id')
                            ->leftJoin('provinces','provinces.id', 'm_wilayah_lembaga.prov_id')
                            ->leftJoin('cities','cities.id', 'm_wilayah_lembaga.city_id')
                            ->leftJoin('districts','districts.id', 'm_wilayah_lembaga.dis_id')
                            ->leftJoin('subdistricts','subdistricts.id', 'm_wilayah_lembaga.subdis_id')
                            ->where(
                                function($query) use ($request){
                                return $query->orWhere('m_wilayah_lembaga.subdis_id', 'LIKE', '%' .$request->subdis_id. '%')
                                             ->orWhere('m_wilayah_lembaga.dis_id', 'LIKE', '%' .$request->dis_id. '%')
                                             ->orWhere('m_wilayah_lembaga.city_id', 'LIKE', '%' .$request->city_id. '%');
                                    
                                })
                            ->get();
            if($data){
                return response()->json(['status'=>true, 'message' => 'data lembaga', 'data'=> $data]);
            }else{
                return response()->json(['status'=>false, 'message' => 'data lembaga tidak ditemukan', 'data'=> $data]);
            }

            
        } catch (\Exception $e) {
            //throw $th;
                return response()->json(['status'=>false, 'error' => 'error', 'data'=> $e]);
        }
       
    }
}
