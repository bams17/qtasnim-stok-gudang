<?php

namespace App\Http\Controllers\API\Wilayah;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Str;
use File;
use DB;
use Mail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class wilayahController extends Controller
{
    //
    public function kota(Type $var = null)
    {
      
        try {
            $data = DB::table('m_kota')
                                ->get();

            return response()->json(['status'=>true, 'message' => 'data kota', 'data'=> $data]);
        } catch (\Exception $e) {
            //throw $th;
                return response()->json(['status'=>false, 'error' => 'error', 'data'=> $e]);
        }


    }

    public function kecamatan(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'id_kota' => 'required',
        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        try {
            $data_id = DB::table('m_kota')
                            ->where('kota', $request->id_kota)
                            ->first();

            $data = DB::table('m_kecamatan')
                            ->where('id_kota', $data_id->id)
                            ->orderBy('kecamatan', 'asc')
                            ->get();

            return response()->json(['status'=>true, 'message' => 'data kecamatan', 'data'=> $data]);
        } catch (\Exception $e) {
            //throw $th;
                return response()->json(['status'=>false, 'error' => 'error', 'data'=> $e]);
        }
    }

    public function desa(Request $request)
    {
        $validator = Validator::make($request->all(), array(
            'id_kecamatan' => 'required',
        ));

        if($validator->fails()){

            $response = array(
                "status"   => false,
                "message"   => "Tidak bisa melakukan proses data",
                "data"      => $validator->errors()->first()
            );
            return response()->json($response);
        }

        try {
            $data = DB::table('m_kecamatan') 
                            ->where('kecamatan', $request->id_kecamatan)
                            ->first();
        
            $data = DB::table('m_desa')
                        ->where('id_kecamatan', $data->id)
                        ->orderBy('desa', 'asc')
                        ->get();

            return response()->json(['status'=>true, 'message' => 'data desa', 'data'=> $data]);
        } catch (\Exception $e) {
            //throw $th;
                return response()->json(['status'=>false, 'error' => 'error', 'data'=> $e]);
        }
    }

    public function getDataLokasi(Type $var = null)
    {
        # code...
        $data = DB::table('m_user')
                    ->select('m_user.link_gambar','m_user.uuid','m_user.nama', 'm_canvasser_lokasi.latitude','m_canvasser_lokasi.longitude', 'm_canvasser.id',)
                    ->leftJoin('m_canvasser','m_canvasser.uuid_user','m_user.uuid')
                    ->leftJoin('m_canvasser_lokasi','m_canvasser_lokasi.canvasser_id','m_canvasser.id')
                    ->whereNotNull('m_canvasser_lokasi.latitude')
                    ->groupBy('m_user.uuid')
                    ->orderBy('m_canvasser.id', 'desc')
                    ->get();
        return response()->json(['status'=>false, 'error' => 'error', 'data'=> $data]);
    }

}
