<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Auth;

class AuthAdministrator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // dd($request);

        if(Auth::guard('admin')->check()!=true){
            
            return redirect('/');
        }
        
        return $next($request);
    }
}
