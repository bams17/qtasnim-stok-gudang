<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class Submenu extends Model
{
    //
    protected $table = 'm_submenu';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'submenu',
        'url',
        'menu_id'
      ];
}
