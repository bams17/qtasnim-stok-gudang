<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class Menus extends Model
{
    protected $table = 'm_menu';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'menu',
        'url',
        'type'
      ];
}
