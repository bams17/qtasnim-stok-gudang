<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Http\Request;
use DB;

class UsersExport implements FromCollection,WithHeadings
{

    protected $search;

    function __construct($search) {
            $this->search = $search;

    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {

        DB::statement(DB::raw('set @no=0'));
        $data = DB::table('m_canvasser')
                ->select(
                    \DB::raw('@no:=@no+1'),
                    'm_user.nama as nama_relawan',
                    'm_canvasser.nama_warga',
                    'm_canvasser.nik',
                    'm_canvasser.nomor_hp',
                    'm_canvasser_lokasi.kota',
                    'm_canvasser_lokasi.kecamatan',
                    'm_canvasser_lokasi.desa',
                    'm_canvasser_lokasi.rw',
                    'm_canvasser_lokasi.rt',
                    'm_canvasser.created_at'
                    )
                ->leftJoin('m_user', 'm_user.uuid', 'm_canvasser.uuid_user')
                ->leftJoin('m_canvasser_lokasi','m_canvasser_lokasi.canvasser_id', 'm_canvasser.id')
                ->where('m_canvasser.type_laporan', 1)
                ->where('m_canvasser.status_laporan', 1)
                ->where(
                    function($query) {
                    return $query->orWhere('m_user.nama', 'LIKE', '%' .$this->search. '%')
                        ->orWhere('m_canvasser.nik', 'LIKE', '%' .$this->search. '%')
                        ->orWhere('m_canvasser.nama_warga', 'LIKE', '%' .$this->search. '%')
                        ->orWhere('m_canvasser_lokasi.kecamatan', 'LIKE', '%' .$this->search. '%')
                        ->orWhere('m_canvasser_lokasi.kota', 'LIKE', '%' .$this->search. '%')
                        ->orWhere('m_canvasser_lokasi.desa', 'LIKE', '%' .$this->search. '%')
                        ->orWhere('m_canvasser_lokasi.desa', 'LIKE', '%' .$this->search. '%')
                        ->orWhere('m_canvasser.kode_laporan', 'LIKE', '%' .$this->search. '%');
                    })
                ->orderBy('m_canvasser.id', 'desc')
                ->get();

        return $data;

    }

    public function headings(): array
    {

        return [
            'No',
            'Nama relawan',
            'Nama warga',
            'NIK',
            'No Hp',
            'Kota/Kab',
            'Kecamatan',
            'Desa',
            'RT/RW',
            'No TPS',
            'Tanggal Laporan'
        ];

       
    }
}
