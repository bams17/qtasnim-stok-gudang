<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>QTasnim</title>
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  <link rel="icon" type="image/png" href="{{asset('assets/admin/assets/img/logoharveo.png')}}">

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{asset('assets/admin/node_modules/jqvmap/dist/jqvmap.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/admin/node_modules/weathericons/css/weather-icons.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/admin/node_modules/weathericons/css/weather-icons-wind.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/admin/node_modules/summernote/dist/summernote-bs4.css')}}">
  <link rel="stylesheet" href="{{asset('assets/admin/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/admin/node_modules/datatables.net-select-bs4/css/select.bootstrap4.min.css')}}">

  <link rel="stylesheet" href="{{asset('assets/admin/node_modules/selectric/public/selectric.css')}}">
  <link rel="stylesheet" href="{{asset('assets/admin/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}">
  <link rel="stylesheet" href="{{asset('assets/admin/node_modules/ionicons201/css/ionicons.min.css')}}">


  

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{asset('assets/admin/assets/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('assets/admin/assets/css/components.css')}}">
  <link rel="stylesheet" href="{{asset('assets/error/assets/css/alerts-css.min.css')}}">


  {{-- CDN --}}
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" >
  <style>
    body.sidebar-mini .main-sidebar .sidebar-menu>li.active>a {
        box-shadow: 0 4px 8px #acb5f6;
        background-color:linear-gradient(0deg, rgba(14, 99, 220, 0.03), rgba(14, 99, 220, 0.03)),
linear-gradient(0deg, #F8FAFE, #F8FAFE);
;
        color: #fff;
    }

    .main-sidebar .sidebar-menu li.active a {
      color: linear-gradient(0deg, rgba(14, 99, 220, 0.03), rgba(14, 99, 220, 0.03)), linear-gradient(0deg, #F8FAFE, #F8FAFE);
        font-weight: 600;
        background-color: #f8fafb;
    }

    
    .nav-link{
      color: linear-gradient(0deg, rgba(14, 99, 220, 0.03), rgba(14, 99, 220, 0.03)),linear-gradient(0deg, #F8FAFE, #F8FAFE);
        text-decoration: none;
        background-color: transparent;
    }
  </style>

</head>

<body>
  <div id="app">
    <div class="main-wrapper">
        @include('layout.admin-layout.header')
    

        @include('layout.admin-layout.sidebar')

      <!-- Main Content -->
       {{-- <div class="contens"> --}}
           @yield('contens')
       {{-- </div> --}}
    


    @include('layout.admin-layout.sidebar')
    
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="{{asset('assets/admin/assets/js/stisla.js')}}"></script>

   <!-- JS Libraies -->
   <script src="{{asset('assets/admin/node_modules/simpleweather/jquery.simpleWeather.min.js')}}"></script>
   <script src="{{asset('assets/admin/node_modules/chart.js/dist/Chart.min.js')}}"></script>
   <script src="{{asset('assets/admin/node_modules/jqvmap/dist/jquery.vmap.min.js')}}"></script>
   <script src="{{asset('assets/admin/node_modules/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>
   <script src="{{asset('assets/admin/node_modules/summernote/dist/summernote-bs4.js')}}"></script>
   <script src="{{asset('assets/admin/node_modules/chocolat/dist/js/jquery.chocolat.min.js')}}"></script>

    <!-- JS Libraies -->
  <script src="{{asset('assets/admin/node_modules/selectric/public/jquery.selectric.min.js')}}"></script>
  <script src="{{asset('assets/admin/node_modules/jquery_upload_preview/assets/js/jquery.uploadPreview.min.js')}}"></script>
  <script src="{{asset('assets/admin/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>

   <script src="{{asset('assets/admin/node_modules/datatables/media/js/jquery.dataTables.min.js')}}"></script>
   <script src="{{asset('assets/admin/node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
   <script src="{{asset('assets/admin/node_modules/datatables.net-select-bs4/js/select.bootstrap4.min.js')}}"></script>
   <script src="{{asset('assets/error/assets/js/alerts.js')}}"></script>


   {{-- CND JS --}}
   <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>


  <!-- Template JS File -->
  <script src="{{asset('assets/admin/assets/js/scripts.js')}}"></script>
  <script src="{{asset('assets/admin/assets/js/custom.js')}}"></script>

  <!-- Page Specific JS File -->
  {{-- <script src="{{asset('assets/admin/assets/js/page/index-0.js')}}"></script> --}}
</body>
      <script type="text/javascript">
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
      </script>
      @yield('script')
</html>


