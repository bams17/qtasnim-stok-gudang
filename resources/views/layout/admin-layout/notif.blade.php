<style>
    .icon-alert{
        height: 50px;
        width: 50px;
        padding: 20px;
        border-radius: 50%;
        background:#c13515;
        color: #fff;
    }
    .alert {
        border-radius: 15px;
        transition: .5s ease-in-out;
        visibility: visible;
        opacity: 1;
        height: auto;
        overflow: hidden;
        animation-name: showAlert;
        animation-duration: .5s;
        animation-fill-mode: both;
        
    }
    .alert .alert--icon {
        height: 50px;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-pack: center;
        justify-content: center;
        -ms-flex-align: center;
        align-items: center;
        font-size: 20px;
    }

    .alert {
        padding: 10px;
        width: 100%;
        border: 1px solid;
        padding: 15px;
        position: relative;
        border-left: 1px solid #bababa;
        background: #ffffff;
        color: #a1a1a1;
        border-radius: 4px;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-pack: center;
        justify-content: center;
        -ms-flex-align: center;
        align-items: center;
        text-align: left;
        font-size: 16px;
    }
</style>
@if (session()->has('success'))
    <div class="icon-alert alert alert_sm alert-fade alert_success col-md-6" style="animation-delay: .1s; border-radius: 15px;" data-fade-time="3">
        <div class="icon-alert alert--icon">
            <i class="fas fa-bell"></i>
        </div>
        <div class="alert--content">
            {{session()->get('success')}}
        </div>
        <div class="alert--close">
        <i class="far fa-times-circle"></i>
        </div>
    </div>
@elseif(session()->has('erorr'))
    <div class="alert alert_sm alert-fade alert_danger col-md-6" style="animation-delay: .1s; border-radius: 15px;" data-fade-time="3">
        <div class="icon-alert alert--icon">
            <i class="fas fa-bell"></i>
        </div>
        <div class="alert--content">
            {{session()->get('erorr')}}
        </div>
        <div class="alert--close">
        <i class="far fa-times-circle"></i>
        </div>
    </div>
@elseif(session()->has('info'))
    <div class="alert alert_sm alert-fade alert_infp col-md-6" style="animation-delay: .1s; border-radius: 15px;" data-fade-time="3">
        <div class="alert--icon icon-alert">
            <i class="fas fa-bell"></i>
        </div>
        <div class="alert--content">
            {{session()->get('info')}}
        </div>
        <div class="alert--close">
        <i class="far fa-times-circle"></i>
        </div>
    </div>
@elseif(session()->has('info-user'))
    <div class="alert alert_sm alert-fade  col-md-6" style="animation-delay: .1s; border-radius: 15px;" data-fade-time="3">
        <div class="icon-alert alert--icon">
            <i class="bi bi-bell-fill"></i>
        </div>
        <div class="alert--content">
            Silahkan untuk mencoba kembali
            <br>
            {{session()->get('info-user')}}
        </div>
        <div class="alert--close">
        <i class="far fa-times-circle"></i>
        </div>
    </div>
@endif

