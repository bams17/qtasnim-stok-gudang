<div class="main-sidebar">
    <aside id="sidebar-wrapper">
      <div class="sidebar-brand">
        <a href="index.html">Qtasnim</a>
      </div>
      <div class="sidebar-brand sidebar-brand-sm">
        <a href="index.html">QT</a>
      </div>
      <ul class="sidebar-menu">
          <li class="menu-header">Dashboard</li>
          <li class="nav-item dropdown {{$dashboard??''}}">
            <a href="#" class="nav-link has-dropdown"><i class="far fa-user"></i><span>Manage Admin</span></a>
            <ul class="nav dropdown-menu">
              <li>
                <a class="nav-link" href="/admin/dashboard/manage-admin">Tambah Pengelola</a>
              </li>
              <!-- <li>
                <a class="nav-link" href="/admin/dashboard/manage-menu">Manage Menu Front end</a>
              </li> -->
            </ul>
          </li>
          <li class="menu-header">Stok</li>
          <li class="nav-item dropdown {{$harga??''}}">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-warehouse"></i><span>Manage Stok</span></a>
            <ul class="nav dropdown-menu">
              <li>
                <a class="nav-link" href="/admin/dashboard/manage-stok-barang">Stok Barang</a>
              </li>
              <li>
                <a class="nav-link" href="/admin/dashboard/manage-stok-barang/inbond">Inbound</a>
              </li>
              <li>
                <a class="nav-link" href="/admin/dashboard/manage-stok-barang/outbond">Outbound</a>
              </li>
            </ul>
          </li>
          <li class="menu-header">Master</li>
          <li class="nav-item dropdown {{$harga??''}}">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-store"></i><span>Manage Barang</span></a>
            <ul class="nav dropdown-menu">
              <li>
                <a class="nav-link" href="/admin/dashboard/manage-produk">Produk</a>
              </li>
              <li>
                <a class="nav-link" href="/admin/dashboard/manage-harga">Harga</a>
              </li>
            </ul>
          </li>
         
          {{-- Menu side bar --}}
        </ul>
        
    </aside>
  </div>