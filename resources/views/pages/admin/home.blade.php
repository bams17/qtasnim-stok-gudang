@extends('layout.admin-layout.app')
@section('contens')

<style>
  
</style>
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Dashboard</h1>
    </div>
    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-3 col-12">
        <div class="card card-statistic-1">
          <div class="card-icon bg-primary">
            <i class="far fa-user"></i>
          </div>
          <div class="card-wrap">
            <div class="card-header">
              <h4>Visitor Now</h4>
            </div>
            <div class="card-body">
              {{isset($visitors_now[0]['visitors'])?$visitors_now[0]['visitors']:0}}
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 col-12">
        <div class="card card-statistic-1">
          <div class="card-icon bg-primary">
            <i class="far fa-user"></i>
          </div>
          <div class="card-wrap">
            <div class="card-header">
              <h4>Total Pageview</h4>
            </div>
            <div class="card-body">
              {{isset($users_pageviews[0][0])?$users_pageviews[0][0]:0}}
            </div>
          </div>
        </div>
      </div>

      @foreach ($users_type as $row)
      <div class="col-lg-3 col-md-3 col-sm-3 col-12">
        <div class="card card-statistic-1">
          <div class="card-icon bg-danger">
            <i class="far fa-newspaper"></i>
          </div>
          <div class="card-wrap">
            <div class="card-header">
              <h4>{{isset($row[0])?$row[0]:0}}</h4>
            </div>
            <div class="card-body">
              {{isset($row[1])?$row[1]:0}}
            </div>
          </div>
        </div>
      </div>
      @endforeach
    </div>
    {{-- <div class="container"> --}}
      <div class="row" style="height:100%">
        <div class="col-lg-6 col-md-6 col-12 col-sm-12">
          <div class="card">
            <div class="card-header">
              <h4>Visitor</h4>
              <div class="card-header-action">
                
              </div>
            </div>
            <div class="card-body">
              <canvas id="myChart" height="182"></canvas>
            </div>
          </div>
          <div class="card">
            <div class="card-header">
              <h4>Popular Browser</h4>
            </div>
            
            <div class="card-body">
              <div class="row">
                @foreach ($browser_top as $row)
                <div class="col text-center">
                  <div class="browser browser-{{ strtolower($row[0])}}"></div>
                  <div class="mt-2 font-weight-bold">{{$row[0]}}</div>
                  <div class="text-muted text-small"><span class="text-primary"><i class="fas fa-caret-up"></i></span> {{$row[1]}} Session</div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-12 col-sm-12">
          <div class="card">
            <div class="card-header">
              <h4>Most popular pages with title breakdown</h4>
            </div>
            <div class="card-body p-2">
              <div class="table-responsive">
                <table class="table table-striped mb-0" id="tabel-visitor">
                  <thead>
                    <tr>
                      <th>Title</th>
                      <th>Url</th>
                      <th>Visitor</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($pages_top as $row)
                    <tr>
                      <td>
                       {{$row[0]}}
                        <div class="table-links">
                        <a href="{{$row[1]}}">Kunjungi</a>
                        </div>
                      </td>
                      <td>
                        {{$row[1]}}
                      </td>
                      <td>
                        {{$row[2]}}
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    {{-- </div> --}}
  </section>
</div>
@endsection
@section('script')
<script type="text/javascript">
   $(function() {
     $('#tabel-visitor').DataTable({
      "pageLength": 6,
      "searching": false,
      "lengthChange": false,
      "order": [[ 2, "desc" ]]
     })
    

     var dataVisitor = {!!json_encode($total_visitors)!!}

     console.log(dataVisitor);

     var tanggalTabel = [];
     var visitorTabel = [];
     var PageViews = []

     for (let i = 0; i < dataVisitor.length; i++) {
      tanggalTabel.push(timeConverter(dataVisitor[i].date));
      visitorTabel.push(dataVisitor[i].visitors)
      PageViews.push(dataVisitor[i].pageViews)
     }

     console.log("Tanggal", tanggalTabel);
     var ctx = document.getElementById("myChart").getContext('2d');

      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: tanggalTabel,
          datasets: [{
            label: 'Visitor',
            data: visitorTabel,
            borderWidth: 2,
            backgroundColor: 'rgba(63,82,227,.8)',
            borderWidth: 0,
            borderColor: 'transparent',
            pointBorderWidth: 0,
            pointRadius: 3.5,
            pointBackgroundColor: 'transparent',
            pointHoverBackgroundColor: 'rgba(63,82,227,.8)',
          },
          {
            label: 'Pageview',
            data: PageViews,
            borderWidth: 2,
            backgroundColor: 'rgba(254,86,83,.7)',
            borderWidth: 0,
            borderColor: 'transparent',
            pointBorderWidth: 0 ,
            pointRadius: 3.5,
            pointBackgroundColor: 'transparent',
            pointHoverBackgroundColor: 'rgba(254,86,83,.8)',
          }
         ]
        },
        options: {
          legend: {
            display: false
          },
          scales: {
            yAxes: [{
              gridLines: {
                // display: false,
                drawBorder: false,
                color: '#f2f2f2',
              },
              ticks: {
                beginAtZero: true,
                // stepSize: 100,
                callback: function(value, index, values) {
                  return  value;
                }
              }
            }],
            xAxes: [{
              gridLines: {
                display: false,
                tickMarkLength: 15,
              }
            }]
          },
        }
      });
   });

   function timeConverter(UNIX_timestamp){
        var a = new Date(UNIX_timestamp);
        var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        var year = a.getFullYear();
        var month = months[a.getMonth()];
        var date = a.getDate();
        var hour = a.getHours();
        var min = a.getMinutes();
        var sec = a.getSeconds();
        var time = date + ' ' + month + ' ' + year  ;
        return time;
}
</script>
@endsection