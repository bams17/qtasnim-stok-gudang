
@extends('layout.admin-layout.app')
@section('contens')
<style> 
.sukses-message {

    animation: fadeOut  forwards;
    animation-delay: 3s; 
}

@keyframes fadeOut {
    from {
      opacity: 1;
    }

    to {
      opacity: 0;
      height: 0;
    }
}
</style>
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <div class="section-header-back">
        <a href="/admin/dashboard/manage-admin" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
      </div>
      <h1>Upadate Pengelola Administrator</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="#">Posts</a></div>
        <div class="breadcrumb-item">Create New Post</div>
      </div>
    </div>

    <div class="section-body">
      @if (count($errors) > 0)

          <div class="alert alert-danger alert-has-icon sukses-message">
            <div class="alert-icon">
              <i class="far fa-lightbulb"></i>
            </div>
            <div class="alert-body">
              <div class="alert-title">Error</div>
              <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          </div>

        @else
        <h2 class="section-title">Admministrator</h2>
        <p class="section-lead">
          Upadate profile admin
        </p>
      @endif
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h4>Upadate</h4>
            </div>
            <form method="POST" action="/admin/dashboard/manage-admin/update/{{$admin->id}}" method="POST" enctype="multipart/form-data">
              @csrf
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <div class="card-body">
              <div class="form-group row mb-4">
                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
                <div class="col-sm-12 col-md-7">
                  <input type="text" name="nama" class="form-control" value="{{$admin->nama}}">
                </div>
              </div>
              <div class="form-group row mb-4">
                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Email</label>
                <div class="col-sm-12 col-md-7">
                  <input type="email" name="email" class="form-control" value="{{$admin->email}}">
                </div>
              </div>
              <div class="form-group row mb-4">
                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Password baru</label>
                <div class="col-sm-12 col-md-7">
                  <input type="password" name="password" class="form-control">
                </div>
              </div>
              <div class="form-group row mb-4">
                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Komfirmasi password baru</label>
                <div class="col-sm-12 col-md-7">
                  <input type="password" name="password_confirmation" class="form-control">
                </div>
              </div>
              <div class="form-group row mb-4">
                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Gambar</label>
                <div class="col-sm-12 col-md-7">
                  <div id="image-preview" class="image-preview">
                    <label for="image-upload" id="image-label">Choose File</label>
                    <input type="file" name="link_foto" id="image-upload" />
                  </div>
                </div>
              </div>
              <div class="form-group row mb-4">
                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                <div class="col-sm-12 col-md-7">
                  <button class="btn btn-primary">Update</button>
                </div>
              </div>
            </div>
          </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('script')
<script type="text/javascript">
   $(function() {
      $("select").selectric();
      $.uploadPreview({
          input_field: "#image-upload",   // Default: .image-upload
          preview_box: "#image-preview",  // Default: .image-preview
          label_field: "#image-label",    // Default: .image-label
          label_default: "Choose File",   // Default: Choose File
          label_selected: "Change File",  // Default: Change File
          no_label: false,                // Default: false
          success_callback: null          // Default: null
        });
        $(".inputtags").tagsinput('items');

        toDataURL({!!json_encode($admin->link_foto)!!}, function(dataUrl) {

            $('#image-preview').css('background-image', 'url(' + dataUrl + ')');

          });
    });

    function toDataURL(url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
          var reader = new FileReader();
          reader.onloadend = function() {
            callback(reader.result);
          }
          reader.readAsDataURL(xhr.response);
        };
        xhr.open('GET', url);
        xhr.responseType = 'blob';
        xhr.send();
      }
    function store() {
      var settings = {
                "url": "/admin/auth/store",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
            };

            $.ajax(settings).done(function (response) {
              console.log(response);
            });

    }
</script>
@endsection