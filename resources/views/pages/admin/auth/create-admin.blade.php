
@extends('layout.admin-layout.app')
@section('contens')
<style> 
.sukses-message {
    animation: fadeOut  forwards;
    animation-delay: 3s;
    
}

@keyframes fadeOut {
    from {
      opacity: 1;
    }

    to {
      opacity: 0;
      height: 0;
    }
}
</style>
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <div class="section-header-back">
        <a href="/admin/dashboard/manage-admin" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
      </div>
      <h1>Tambah Pengelola Administrator</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="#">Posts</a></div>
        <div class="breadcrumb-item">Create New Post</div>
      </div>
    </div>

    <div class="section-body">
      {{-- @include('layout.admin-layout.notif') --}}
      <div id="info">
      </div>
        <h2 class="section-title">Admministrator</h2>
        <p class="section-lead">
          kamu bisa menambahkan pengelolaan admin
        </p>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h4>Admin Baru</h4>
            </div>
            <form method="POST" action="/admin/dashboard/manage-admin/store" method="POST" enctype="multipart/form-data" id="adminPost">
              @csrf
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <div class="card-body">
              <div class="form-group row mb-4">
                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
                <div class="col-sm-12 col-md-7">
                  <input type="text" name="nama" class="form-control" value={{old('nama')}}>
                </div>
              </div>
              <div class="form-group row mb-4">
                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Email</label>
                <div class="col-sm-12 col-md-7">
                  <input type="email" name="email" class="form-control" value={{old('email')}}>
                </div>
              </div>
              <div class="form-group row mb-4">
                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Password</label>
                <div class="col-sm-12 col-md-7">
                  <input type="password" name="password" class="form-control">
                </div>
              </div>
              <div class="form-group row mb-4">
                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Komfirmasi Password</label>
                <div class="col-sm-12 col-md-7">
                  <input type="password" name="password_confirmation" class="form-control">
                </div>
              </div>
              <div class="form-group row mb-4">
                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Gambar</label>
                <div class="col-sm-12 col-md-7">
                  <div id="image-preview" class="image-preview">
                    <label for="image-upload" id="image-label">Choose File</label>
                    <input type="file" name="link_foto" id="image-upload" value{{old('link_foto')}}/>
                  </div>
                </div>
              </div>
              <div class="form-group row mb-4">
                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                <div class="col-sm-12 col-md-7">
                  <button class="btn btn-primary">Sumbmit</button>
                </div>
              </div>
            </div>
          </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('script')
<script type="text/javascript">
   $(function() {
      $('#table-1').DataTable();
      $("select").selectric();
      $.uploadPreview({
          input_field: "#image-upload",   // Default: .image-upload
          preview_box: "#image-preview",  // Default: .image-preview
          label_field: "#image-label",    // Default: .image-label
          label_default: "Choose File",   // Default: Choose File
          label_selected: "Change File",  // Default: Change File
          no_label: false,                // Default: false
          success_callback: null          // Default: null
        });
        $(".inputtags").tagsinput('items');

        $('#adminPost').submit(function(e){
            e.preventDefault();
            var form = $(this);

            console.log(form);
           
            $.ajax({
                type: "POST",
                url: "/admin/dashboard/manage-admin/store",
                data: new FormData(form[0]),
                processData: false,
                contentType: false,
                success: function(data){
                    console.log(data);
                    if (data.status==false) {
                          $("#info").html(`<div class="col-md-4 alert alert_sm alert-fade alert_danger" style="animation-delay: .1s" data-fade-time="3">
                                              <div class="alert--icon">
                                                <i class="fas fa-bell"></i>
                                              </div>
                                              <div class="alert--content">
                                                `+data.data+`
                                              </div>
                                              <div class="alert--close">
                                                <i class="far fa-times-circle"></i>
                                              </div>
                                          </div>`);
                          $('.alert--close').click(function () {
                                $('.alert_sm').addClass('alert_none')
                            });           
                    }else if(data.status==true){

                      window.location = "/admin/dashboard/manage-admin?data=sukses";

                    }else{
                      $("#info").html(`<div class="col-md-4 alert alert_sm alert-fade alert_success" style="animation-delay: .1s" data-fade-time="3">
                                              <div class="alert--icon">
                                                <i class="fas fa-bell"></i>
                                              </div>
                                              <div class="alert--content">
                                                `+data.data+`
                                              </div>
                                              <div class="alert--close">
                                                <i class="far fa-times-circle"></i>
                                              </div>
                                          </div>`);
                          $('.alert--close').click(function () {
                                $('.alert_sm').addClass('alert_none')
                            });  
                    }
                }
            });
      });
    });






    function store() {
      var settings = {
                "url": "/admin/auth/store",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
            };

            $.ajax(settings).done(function (response) {
              console.log(response);
            });

    }
</script>
@endsection