
@extends('layout.admin-layout.app')
@section('contens')
<style> 

/*left right modal*/
.modal.left_modal, .modal.right_modal{
  position: fixed;
  z-index: 99999;
}
.modal.left_modal .modal-dialog,
.modal.right_modal .modal-dialog {
  position: fixed;
  margin: auto;
  width: 32%;
  height: 100%;
  -webkit-transform: translate3d(0%, 0, 0);
      -ms-transform: translate3d(0%, 0, 0);
       -o-transform: translate3d(0%, 0, 0);
          transform: translate3d(0%, 0, 0);
}

.modal-dialog {
    /* max-width: 100%; */
    margin: 1.75rem auto;
}
@media (min-width: 576px)
{
.left_modal .modal-dialog {
    max-width: 100%;
}

.right_modal .modal-dialog {
    max-width: 100%;
}
}
.modal.left_modal .modal-content,
.modal.right_modal .modal-content {
  /*overflow-y: auto;
    overflow-x: hidden;*/
    height: 100vh !important;
}

.modal.left_modal .modal-body,
.modal.right_modal .modal-body {
  padding: 15px 15px 30px;
}

/*.modal.left_modal  {
    pointer-events: none;
    background: transparent;
}*/

.modal-backdrop {
    display: none;
}

/*Left*/
.modal.left_modal.fade .modal-dialog{
  left: -50%;
  -webkit-transition: opacity 0.3s linear, left 0.3s ease-out;
  -moz-transition: opacity 0.3s linear, left 0.3s ease-out;
  -o-transition: opacity 0.3s linear, left 0.3s ease-out;
  transition: opacity 0.3s linear, left 0.3s ease-out;
}

.modal.left_modal.fade.show .modal-dialog{
  left: 0;
  /* box-shadow: 0px 0px 19px 
rgba(0,0,0,.5); */
}

/*Right*/
.modal.right_modal.fade .modal-dialog {
  right: -50%;
  -webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
     -moz-transition: opacity 0.3s linear, right 0.3s ease-out;
       -o-transition: opacity 0.3s linear, right 0.3s ease-out;
          transition: opacity 0.3s linear, right 0.3s ease-out;
}



.modal.right_modal.fade.show .modal-dialog {
  right: 0;
  /* box-shadow: 0px 0px 19px 
rgba(0,0,0,.5); */
}

/* ----- MODAL STYLE ----- */
.modal-content {
  border-radius: 0;
  border: none;
}



.modal-header.left_modal, .modal-header.right_modal {

  padding: 10px 15px;
  border-bottom-color: 
#EEEEEE;
  background-color: 
#FAFAFA;
}

.modal_outer .modal-body {
    /*height:90%;*/
    overflow-y: auto;
    overflow-x: hidden;
    height: 91vh;
}

.sukses-message {
    animation: fadeOut  forwards;
    animation-delay: 3s;
    
}

@keyframes fadeOut {
    from {
      opacity: 1;
    }

    to {
      opacity: 0;
      height: 0;
    }
}

</style>
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Pengelolan Admin</h1>
      <div class="section-header-button">
        <a href="/admin/dashboard/manage-admin/create" class="btn btn-primary">Tambah</a>
      </div>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="#">Forms</a></div>
        <div class="breadcrumb-item">Advanced Forms</div>
      </div>
    </div>
    <div class="section-body">
      <h2 class="section-title">Data Admin</h2>
      <p class="section-lead">
        pengelolan administrator
      </p>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              @include('layout.admin-layout.notif')
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-striped" id="table-admin">
                  <thead>
                    <tr>
                      <th class="text-center">
                        konten
                      </th>
                      <th>Name</th>
                      <th>email</th>
                      <th>Foto</th>
                      <th>Date</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($data_admin as $row)
                    <tr>
                      <td>
                        {{$loop->index}}
                      </td>
                      <td>{{$row->nama}}</td>
                      <td class="align-middle">
                        {{$row->email}}
                      </td>
                      <td>
                        <img alt="image" src="data:image/png;base64,{{DNS2D::getBarcodePNG($row->link_foto, 'QRCODE')}}" alt="barcode" class="rounded-circle" width="40" height="40" data-toggle="tooltip" title="{{$row->nama}}">
                      </td>
                      <td>{{$row->created_at}}</td>
                      <td><div class="badge badge-success">Aktive</div></td>
                      <td><a href="/admin/dashboard/manage-admin/edit?id={{$row->id}}" class="btn btn-secondary">Detail</a></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
    {{-- Modal Tambah --}}
    <div class="modal fade right_modal" tabindex="-1" role="dialog" id="exampleModal" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-lg" role="document">
        <form>
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Tambah Admin</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Email address</label>
                  <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"  required="">
                  <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password"  required="">
                </div>
            </div>
            <div class="modal-footer bg-whitesmoke br">
              <button type="submit" class="btn btn-primary" onclick="store()">Save changes</button>
            </div>
          </div>
        </form>
      </div>
    </div>
@endsection
@section('script')
<script type="text/javascript">
   $(function() {
      $('#table-admin').DataTable();
      
    });

    function store() {
      var settings = {
                "url": "/admin/auth/store",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
            };
            $.ajax(settings).done(function (response) {
              console.log(response);
            });

    }
</script>
@endsection