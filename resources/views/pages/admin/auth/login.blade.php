
@extends('layout.admin-layout.auth')
@section('contens')
<section class="section">
      <div class="row row h-100 justify-content-center align-items-center mt-5">
        <div class="col-lg-4 col-md-6 col-12 rder-2 bg-white" style="border-radius:50px">
          <div class="p-4 m-3">
            <div class="text-center" width="100%">
              <img src="{{asset('/assets/admin/assets/img/logoharveonew.png')}}" alt="logo" width="150px" class="mb-5 mt-2">
            </div>
           
            @if (count($errors) > 0)
            <div class="alert alert-danger alert-has-icon sukses-message">
              <div class="alert-icon">
                <i class="far fa-lightbulb"></i>
              </div>
              <div class="alert-body">
                <div class="alert-title">Error</div>
                <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            </div>
          @else
          
          <p class="text-muted">
            Login
          </p>
          @endif
            <form action="/admin/auth/admin-login-auth" method="POST" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="_token" value="{{ csrf_token() }}" />
              <div class="form-group">
                <label for="email">Email</label>
                <input id="email" type="email" class="form-control" name="email" tabindex="1" required autofocus>
                <div class="invalid-feedback">
                  Please fill in your email
                </div>
              </div>
  
              <div class="form-group">
                <div class="d-block">
                  <label for="password" class="control-label">Password</label>
                </div>
                <input id="password" type="password" class="form-control" name="password" tabindex="2" required>
                <div class="invalid-feedback">
                  please fill in your password
                </div>
              </div>
  
              <div class="form-group">
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" name="remember" class="custom-control-input" tabindex="3" id="remember-me">
                  <label class="custom-control-label" for="remember-me">Remember Me</label>
                </div>
              </div>
  
              <div class="form-group text-right">
                <a href="auth-forgot-password.html" class="float-left mt-3">
                  Forgot Password?
                </a>
                <button type="submit" class="btn btn-primary btn-lg btn-icon icon-right" tabindex="4">
                  Login
                </button>
              </div>
            </form>
  
            <div class="text-center mt-5 text-small">
              Copyright &copy; Qtasnim
            </div>
          </div>
        </div>
    </div>
  </section>
@endsection