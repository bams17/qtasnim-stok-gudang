
@extends('layout.admin-layout.app')
@section('contens')
<style> 

/*left right modal*/
.modal.left_modal, .modal.right_modal{
  position: fixed;
  z-index: 99999;
}
.modal.left_modal .modal-dialog,
.modal.right_modal .modal-dialog {
  position: fixed;
  margin: auto;
  width: 32%;
  height: 100%;
  -webkit-transform: translate3d(0%, 0, 0);
      -ms-transform: translate3d(0%, 0, 0);
       -o-transform: translate3d(0%, 0, 0);
          transform: translate3d(0%, 0, 0);
}

.modal-dialog {
    /* max-width: 100%; */
    margin: 1.75rem auto;
}
@media (min-width: 576px)
{
.left_modal .modal-dialog {
    max-width: 100%;
}

.right_modal .modal-dialog {
    max-width: 100%;
}
}
.modal.left_modal .modal-content,
.modal.right_modal .modal-content {
  /*overflow-y: auto;
    overflow-x: hidden;*/
    height: 100vh !important;
}

.modal.left_modal .modal-body,
.modal.right_modal .modal-body {
  padding: 15px 15px 30px;
}

/*.modal.left_modal  {
    pointer-events: none;
    background: transparent;
}*/

.modal-backdrop {
    display: none;
}

/*Left*/
.modal.left_modal.fade .modal-dialog{
  left: -50%;
  -webkit-transition: opacity 0.3s linear, left 0.3s ease-out;
  -moz-transition: opacity 0.3s linear, left 0.3s ease-out;
  -o-transition: opacity 0.3s linear, left 0.3s ease-out;
  transition: opacity 0.3s linear, left 0.3s ease-out;
}

.modal.left_modal.fade.show .modal-dialog{
  left: 0;
  /* box-shadow: 0px 0px 19px 
rgba(0,0,0,.5); */
}

/*Right*/
.modal.right_modal.fade .modal-dialog {
  right: -50%;
  -webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
     -moz-transition: opacity 0.3s linear, right 0.3s ease-out;
       -o-transition: opacity 0.3s linear, right 0.3s ease-out;
          transition: opacity 0.3s linear, right 0.3s ease-out;
}



.modal.right_modal.fade.show .modal-dialog {
  right: 0;
  /* box-shadow: 0px 0px 19px 
rgba(0,0,0,.5); */
}

/* ----- MODAL STYLE ----- */
.modal-content {
  border-radius: 0;
  border: none;
}



.modal-header.left_modal, .modal-header.right_modal {

  padding: 10px 15px;
  border-bottom-color: 
#EEEEEE;
  background-color: 
#FAFAFA;
}

.modal_outer .modal-body {
    /*height:90%;*/
    overflow-y: auto;
    overflow-x: hidden;
    height: 91vh;
}

.sukses-message {
   -webkit-animation: fadeOut 4s linear forwards;
    animation-delay: 3s;
    
}

@keyframes fadeOut {
  50% {
    opacity: 1;
  }
  100% {
    opacity: 0;
    display: none;
  }
}

div.dataTables_wrapper div.dataTables_processing {
    font-size: 0 !important;
    background-image: url(data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJsb2FkZXItMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQogd2lkdGg9IjQwcHgiIGhlaWdodD0iNDBweCIgdmlld0JveD0iMCAwIDUwIDUwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MCA1MDsiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPHBhdGggZmlsbD0iIzAwMCIgZD0iTTQzLjkzNSwyNS4xNDVjMC0xMC4zMTgtOC4zNjQtMTguNjgzLTE4LjY4My0xOC42ODNjLTEwLjMxOCwwLTE4LjY4Myw4LjM2NS0xOC42ODMsMTguNjgzaDQuMDY4YzAtOC4wNzEsNi41NDMtMTQuNjE1LDE0LjYxNS0xNC42MTVjOC4wNzIsMCwxNC42MTUsNi41NDMsMTQuNjE1LDE0LjYxNUg0My45MzV6Ij4NCjxhbmltYXRlVHJhbnNmb3JtIGF0dHJpYnV0ZVR5cGU9InhtbCINCiAgYXR0cmlidXRlTmFtZT0idHJhbnNmb3JtIg0KICB0eXBlPSJyb3RhdGUiDQogIGZyb209IjAgMjUgMjUiDQogIHRvPSIzNjAgMjUgMjUiDQogIGR1cj0iMC42cyINCiAgcmVwZWF0Q291bnQ9ImluZGVmaW5pdGUiLz4NCjwvcGF0aD4NCjwvc3ZnPg0K) !important;
    background-color: #ffffff0d;
    background-size: 100%;
    width: 20px !important;
    height: 20px;
    border: none;
    box-shadow: 0 4px 8px rgb(0 0 0 / 3%);
    top: 50% !important;
    left: 50% !important;
    transform: translate(-50%, -50%) !important;
    margin: 0 !important;
    opacity: 1 !important;
}

</style>
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Manage Harga</h1>
      <div class="section-header-button">
        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >Tambah</button>
      </div>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="#">Forms</a></div>
        <div class="breadcrumb-item">Advanced Forms</div>
      </div>
    </div>
    <div class="section-body">
      <h2 class="section-title">Harga Terkini</h2>
      <!-- <p class="section-lead">
        List User
      </p> -->
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
               <div class="col-md-4 info-alert">

               </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-striped" id="table-1">
                  <thead>
                    <tr>
                      <th class="text-center">
                        No
                      </th>
                      <th>Kode</th>
                      <th>Nama Produk</th>
                      <th>Jenis</th>
                      <th>Harga</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
    {{-- Modal Tambah --}}
    <div class="modal fade right_modal" tabindex="-1" role="dialog" id="exampleModal" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-xl" role="document">
        <form id="kategoriPost" enctype="multipart/form-data">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Tambah</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div id="info">

              </div>
                <div class="form-group p-3">
                  <label for="exampleInputEmail1">Nama Varietas</label>
                  <select class="form-control " name="kode_produk" id="nama_produk" required>
                    <option value="">-- Pilih produl --</option>
                    @foreach ($produk as $row)
                      <option value="{{$row->kode_produk}}">{{$row->nama_produk}}</option>
                    @endforeach
                  </select>
                  <small class="form-text text-muted">isi dengan nama produk</small>
                </div>
                <div class="form-group p-3">
                  <label for="exampleInputEmail1">Satua</label>
                  <select class="form-control " name="satua_produk" id="satua_produk" required>
                    <option value="">-- Pilih Jenis --</option>
                    <option value="Konsumsi">Konsumsi</option>
                    <option value="Pembersih">Pembersih</option>
                  </select>
                  <small class="form-text text-muted">isi dengan satua</small>
                </div>
                <div class="form-group p-3">
                  <label for="exampleInputEmail1">Harga</label>
                  <input type="number" class="form-control" name="harga_produk" required="">
                  <small class="form-text text-muted">isi dengan harga</small>
                </div>
            </div>
            <div class="modal-footer bg-whitesmoke br">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
        </form>
      </div>
    </div>

    {{-- Modal Edit --}}
    <div class="modal fade right_modal" tabindex="-1" role="dialog" id="exampleModalEdit" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-lg" role="document">
        <form id="kategoriPostEdit">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Edit Kota</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div id="infoEdit">

              </div>
              <div class="form-group p-3">
                  <label for="exampleInputEmail1">Nama Varietas</label>
                  <select class="form-control " name="kode_produk" id="edit_nama_produk" required readonly>
                    <option value="">-- Pilih produl --</option>
                    @foreach ($produk as $row)
                      <option value="{{$row->kode_produk}}">{{$row->nama_produk}}</option>
                    @endforeach
                  </select>
                  <small class="form-text text-muted">isi dengan nama Barang</small>
                </div>
                <div class="form-group p-3">
                  <label for="exampleInputEmail1">Satua</label>
                  <select class="form-control " name="satua_produk" id="edit_satua_produk" required>
                    <option value="">-- Pilih Jenis --</option>
                    <option value="Konsumsi">Konsumsi</option>
                    <option value="Pembersih">Pembersih</option>
                  </select>
                  <small class="form-text text-muted">isi dengan Jenis Barang</small>
                </div>
                <div class="form-group p-3">
                  <label for="exampleInputEmail1">Harga</label>
                  <input type="number" class="form-control" name="harga_produk"  id="edit_harga_produk" required="">
                  <small class="form-text text-muted">isi dengan harga</small>
                </div>
   
            </div>
            <div class="modal-footer bg-whitesmoke br">
              <button type="submit" class="btn btn-primary">Update</button>
            </div>
          </div>
        </form>
      </div>
    </div>


@endsection
@section('script')
<script type="text/javascript">
var rows_selected = [];
let NoForm = 0;
   $(function() {
    dataTabel();
    $('#kategoriPost').submit(function(e){
        e.preventDefault();
        // var form = $(this).serialize();
        var form = new FormData(this);

        console.log(form);
        $.ajax({
            type: "POST",
            url: "/admin/dashboard/manage-harga/store",
            data: form,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data){
                console.log(data);
                if (data.status==false) {
                      $("#info").html(`<div class="alert alert_sm alert-fade alert_danger" style="animation-delay: .1s" data-fade-time="3">
                                          <div class="alert--icon">
                                            <i class="fas fa-bell"></i>
                                          </div>
                                          <div class="alert--content">
                                            `+data.data+`
                                          </div>
                                          <div class="alert--close">
                                            <i class="far fa-times-circle"></i>
                                          </div>
                                      </div>`);
                      $('.alert--close').click(function () {
                            $('.alert_sm').addClass('alert_none')
                        });           
                }else{
                  $('#table-1').DataTable().destroy();
                  dataTabel();
                  $('#exampleModal').modal('hide');
                  $(".info-alert").html(`<div class="alert alert_sm alert-fade alert_success" style="animation-delay: .1s" data-fade-time="3">
                                          <div class="alert--icon">
                                            <i class="fas fa-bell"></i>
                                          </div>
                                          <div class="alert--content">
                                            `+data.data+`
                                          </div>
                                          <div class="alert--close">
                                            <i class="far fa-times-circle"></i>
                                          </div>
                                      </div>`);
                      $('.alert--close').click(function () {
                            $('.alert_sm').addClass('alert_none')
                        });  
                }
            }
        });
      });



      $('#kategoriPostEdit').submit(function(e){
        e.preventDefault();
        var form = $(this).serializeArray();
        
        $.ajax({
            type: "POST",
            url: "/admin/dashboard/manage-harga/update",
            data: form,
            success: function(data){
                console.log(data);
                if (data.status==false) {
                      $("#infoEdit").html(`<div class="alert alert_sm alert-fade alert_danger" style="animation-delay: .1s" data-fade-time="3">
                                          <div class="alert--icon">
                                            <i class="fas fa-bell"></i>
                                          </div>
                                          <div class="alert--content">
                                            `+data.data+`
                                          </div>
                                          <div class="alert--close">
                                            <i class="far fa-times-circle"></i>
                                          </div>
                                      </div>`);
                      $('.alert--close').click(function () {
                            $('.alert_sm').addClass('alert_none')
                        });           
                }else{
                  $('#table-1').DataTable().destroy();
                  dataTabel();
                  $('#exampleModalEdit').modal('hide');

                  $(".info-alert").html(`<div class="alert alert_sm alert-fade alert_success" style="animation-delay: .1s" data-fade-time="3">
                                          <div class="alert--icon">
                                            <i class="fas fa-bell"></i>
                                          </div>
                                          <div class="alert--content">
                                            `+data.data+`
                                          </div>
                                          <div class="alert--close">
                                            <i class="far fa-times-circle"></i>
                                          </div>
                                      </div>`);
                      $('.alert--close').click(function () {
                            $('.alert_sm').addClass('alert_none')
                        });  
                }
            }
        });
      });

      $('#kategoriPostHapus').submit(function(e){
        e.preventDefault();
        var form = $(this).serializeArray();
        
        $.ajax({
            type: "POST",
            url: "/admin/dashboard/manage-user/hapus/tim",
            data: form,
            success: function(data){
                console.log(data);
                if (data.status==false) {
                      $("#infoHapus").html(`<div class="alert alert_sm alert-fade alert_danger" style="animation-delay: .1s" data-fade-time="3">
                                          <div class="alert--icon">
                                            <i class="fas fa-bell"></i>
                                          </div>
                                          <div class="alert--content">
                                            `+data.data+`
                                          </div>
                                          <div class="alert--close">
                                            <i class="far fa-times-circle"></i>
                                          </div>
                                      </div>`);
                      $('.alert--close').click(function () {
                            $('.alert_sm').addClass('alert_none')
                        });           
                }else{
                  $('#table-1').DataTable().destroy();
                  dataTabel();
                  $('#exampleModalHapus').modal('hide');

                  $(".info-alert").html(`<div class="alert alert_sm alert-fade alert_success" style="animation-delay: .1s" data-fade-time="3">
                                          <div class="alert--icon">
                                            <i class="fas fa-bell"></i>
                                          </div>
                                          <div class="alert--content">
                                            `+data.data+`
                                          </div>
                                          <div class="alert--close">
                                            <i class="far fa-times-circle"></i>
                                          </div>
                                      </div>`);
                      $('.alert--close').click(function () {
                            $('.alert_sm').addClass('alert_none')
                        });  
                }
            }
        });
      });

      $('#table-1 tbody').on('click', 'td.btn-detail', function () {

          var tr = $(this).closest('tr');
          var row = $('#table-1').DataTable().row( tr );

          console.log(row.data());
          $('#edit_nama_produk').val(row.data().kode_produk);
          $('#edit_satua_produk').val(row.data().satua_produk);
          $('#edit_harga_produk').val(row.data().harga_produk)


          
          $('#hapusKategori').val(row.data().nama_tim);
          $('#hapusKategoriId').val(row.data().id);
          
      });


      
    });

    function dataTabel() {
      var table = $('#table-1').DataTable({
                  "processing": true,
                  "bDestroy": true,
                  // "bFilter": false, 
                  "bInfo": true,
                  "order":[0,"asc"],
                  "bSortable": false,
                  "bPaginate": true,
                  "bLengthChange": false,
                  // "bFilter": false,
                  "retrieve": true, 
                  ajax : {
                      url : "/admin/dashboard/manage-harga/datatabel",
                      type : "GET",
                      method : "GET",
                      dataSrc : "",
                  },
                  "columnDefs" : [
                            {
                                "targets": 0,
                                // "visible": false,
                                "className": "text-center",
                                "width": "5%" ,
                                render:function(data,type,row,meta){

                                    return  meta.row + meta.settings._iDisplayStart+1

                                }
                            },
                            {
                                "targets": 1,
                                // "visible": false,
                                // "width": "5%" ,
                                render:function(data,type,row,meta){
                                  
                                    return  row.kode_produk
                                }
                            },
                            {
                                "targets": 2,
                                // "visible": false,
                                // "width": "5%" ,
                                render:function(data,type,row,meta){
                                  
                                    return  row.nama_produk
                                }
                            },
                            {
                                "targets": 3,
                                // "visible": false,
                                // "width": "5%" ,
                                render:function(data,type,row,meta){
                                  
                                    return  row.satua_produk
                                }
                            },
                            {
                                "targets": 4,
                                // "visible": false,
                                // "width": "5%" ,
                                render:function(data,type,row,meta){
                                  
                                    return  row.harga_produk
                                }
                            },
                            {
                                "targets":5,
                                // "visible": false,
                                "className": "btn-detail",
                                "width": "10%" ,
                                render:function(data,type,row,meta){
                                  
                                    return  `<button class="btn btn-icon btn-sm btn-info btn-detail"  data-toggle="modal" data-target="#exampleModalEdit"><li class="ion ion-edit" data-pack="default" data-tags=""></li></button>`
                                }
                            },
                          ],
        }                
      );

    }
    
</script>
@endsection