
@extends('layout.admin-layout.app')
@section('contens')
<style> 

/*left right modal*/
.modal.left_modal, .modal.right_modal{
  position: fixed;
  z-index: 99999;
}
.modal.left_modal .modal-dialog,
.modal.right_modal .modal-dialog {
  position: fixed;
  margin: auto;
  width: 32%;
  height: 100%;
  -webkit-transform: translate3d(0%, 0, 0);
      -ms-transform: translate3d(0%, 0, 0);
       -o-transform: translate3d(0%, 0, 0);
          transform: translate3d(0%, 0, 0);
}

.modal-dialog {
    /* max-width: 100%; */
    margin: 1.75rem auto;
}
@media (min-width: 576px)
{
.left_modal .modal-dialog {
    max-width: 100%;
}

.right_modal .modal-dialog {
    max-width: 100%;
}
}
.modal.left_modal .modal-content,
.modal.right_modal .modal-content {
  /*overflow-y: auto;
    overflow-x: hidden;*/
    height: 100vh !important;
}

.modal.left_modal .modal-body,
.modal.right_modal .modal-body {
  padding: 15px 15px 30px;
}

/*.modal.left_modal  {
    pointer-events: none;
    background: transparent;
}*/

.modal-backdrop {
    display: none;
}

/*Left*/
.modal.left_modal.fade .modal-dialog{
  left: -50%;
  -webkit-transition: opacity 0.3s linear, left 0.3s ease-out;
  -moz-transition: opacity 0.3s linear, left 0.3s ease-out;
  -o-transition: opacity 0.3s linear, left 0.3s ease-out;
  transition: opacity 0.3s linear, left 0.3s ease-out;
}

.modal.left_modal.fade.show .modal-dialog{
  left: 0;
  /* box-shadow: 0px 0px 19px 
rgba(0,0,0,.5); */
}

/*Right*/
.modal.right_modal.fade .modal-dialog {
  right: -50%;
  -webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
     -moz-transition: opacity 0.3s linear, right 0.3s ease-out;
       -o-transition: opacity 0.3s linear, right 0.3s ease-out;
          transition: opacity 0.3s linear, right 0.3s ease-out;
}



.modal.right_modal.fade.show .modal-dialog {
  right: 0;
  /* box-shadow: 0px 0px 19px 
rgba(0,0,0,.5); */
}

/* ----- MODAL STYLE ----- */
.modal-content {
  border-radius: 0;
  border: none;
}



.modal-header.left_modal, .modal-header.right_modal {

  padding: 10px 15px;
  border-bottom-color: 
#EEEEEE;
  background-color: 
#FAFAFA;
}

.modal_outer .modal-body {
    /*height:90%;*/
    overflow-y: auto;
    overflow-x: hidden;
    height: 91vh;
}

.sukses-message {
   -webkit-animation: fadeOut 4s linear forwards;
    animation-delay: 3s;
    
}

hr {
    display: block;
    height: 1px;
    border: 0;
    border-top: 1px solid #ccc;
    margin: 1em 0;
    padding: 0;
}

@keyframes fadeOut {
  50% {
    opacity: 1;
  }
  100% {
    opacity: 0;
    display: none;
  }
}

div.dataTables_wrapper div.dataTables_processing {
    font-size: 0 !important;
    background-image: url(data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJsb2FkZXItMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQogd2lkdGg9IjQwcHgiIGhlaWdodD0iNDBweCIgdmlld0JveD0iMCAwIDUwIDUwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MCA1MDsiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPHBhdGggZmlsbD0iIzAwMCIgZD0iTTQzLjkzNSwyNS4xNDVjMC0xMC4zMTgtOC4zNjQtMTguNjgzLTE4LjY4My0xOC42ODNjLTEwLjMxOCwwLTE4LjY4Myw4LjM2NS0xOC42ODMsMTguNjgzaDQuMDY4YzAtOC4wNzEsNi41NDMtMTQuNjE1LDE0LjYxNS0xNC42MTVjOC4wNzIsMCwxNC42MTUsNi41NDMsMTQuNjE1LDE0LjYxNUg0My45MzV6Ij4NCjxhbmltYXRlVHJhbnNmb3JtIGF0dHJpYnV0ZVR5cGU9InhtbCINCiAgYXR0cmlidXRlTmFtZT0idHJhbnNmb3JtIg0KICB0eXBlPSJyb3RhdGUiDQogIGZyb209IjAgMjUgMjUiDQogIHRvPSIzNjAgMjUgMjUiDQogIGR1cj0iMC42cyINCiAgcmVwZWF0Q291bnQ9ImluZGVmaW5pdGUiLz4NCjwvcGF0aD4NCjwvc3ZnPg0K) !important;
    background-color: #ffffff0d;
    background-size: 100%;
    width: 20px !important;
    height: 20px;
    border: none;
    box-shadow: 0 4px 8px rgb(0 0 0 / 3%);
    top: 50% !important;
    left: 50% !important;
    transform: translate(-50%, -50%) !important;
    margin: 0 !important;
    opacity: 1 !important;
}

</style>
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Stok Barang</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="#">Produk</a></div>
        <div class="breadcrumb-item">Stok</div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-4 col-12">
        <div class="card card-statistic-1">
          <div class="card-icon bg-primary">
            <i class="fas fa-clipboard-list"></i>
          </div>
          <div class="card-wrap">
            <div class="card-header">
              <h4>Total Produk</h4>
            </div>
            <div class="card-body">
              {{$total_produk}}
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-4 col-md-4 col-sm-4 col-12">
        <div class="card card-statistic-1">
          <div class="card-icon bg-primary">
            <i class="fas fa-box"></i>
          </div>
          <div class="card-wrap">
            <div class="card-header">
              <h4>Total Inbound</h4>
            </div>
            <div class="card-body">
              {{$total_inbond}}
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-4 col-md-4 col-sm-4 col-12">
        <div class="card card-statistic-1">
          <div class="card-icon bg-primary">
            <i class="fas fa-box-open"></i>
          </div>
          <div class="card-wrap">
            <div class="card-header">
              <h4>Total Outbound</h4>
            </div>
            <div class="card-body">
              {{$total_outbond}}
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col">
        <div class="card">
            <div class="card-header">
              <h4>Transaksi Barang Populer (Outbound)</h4>
              <div class="card-header-action">
                
              </div>
            </div>
            <div class="card-body">
              <canvas id="myChart" height="100"></canvas>
            </div>
          </div>  
      </div>
    </div>

    <div class="section-body">
      <h2 class="section-title">Stok Barang</h2>
      <!-- <p class="section-lead">
        List User
      </p> -->
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
               <div class="col-md-4 info-alert">

               </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-striped" id="table-1">
                  <thead>
                    <tr>
                      <th class="text-center">
                        No
                      </th>
                      <th>Nama Barang</th>
                      <th>Stok</th>
                      <th>Jumlah Terjual</th>
                      <th>Jenis Barang</th>
                      <th>Tanggal Transaksi</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="exampleModalEdit" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-xl" role="document" style="width: 100%;">
        <form id="kategoriPostEdit">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row d-flex justify-content-center">
                <div class="col">
                  <div class="d-flex justify-content-center">
                    <div>
                      <img class="rounded-circle" width="100px" height="100px" id="link_gambar" src="https://lh3.googleusercontent.com/ogw/AAEL6sg2Vs_RjkvrT_8civ4WgysWlAFRgfsrQ1e2kA3_mg=s64-c-mo">
                      <h5 class="modal-title text-center" id="info-edit"></h5>
                    </div>
                  </div>
                </div>
              </div>
              <div>
                <hr/>
              </div>
                
                <div class="form-group">
                  <div class="row">
                    <div class="col-6">
                      <label for="exampleInputEmail1">Inbound</label>
                       <div class="table-responsive">
                        <table class="table table-striped" id="table-inbond" style="width:100%">
                          <thead>
                            <tr>
                              <th class="text-center">
                                No
                              </th>
                              <th>Invoice</th>
                              <th>Tanggal</th>
                              <th>Qty</th>
                            </tr>
                          </thead>
                        </table>
                      </div>
                    </div>
                    <div class="col-6">
                      <label for="exampleInputEmail1">Outbound</label>
                      <div class="table-responsive">
                        <table class="table table-striped" id="table-outbond" style="width:100%">
                          <thead>
                            <tr>
                              <th class="text-center">
                                No
                              </th>
                              <th>Invoice</th>
                              <th>Tanggal</th>
                              <th>Qty</th>
                            </tr>
                          </thead>
                        </table>
                      </div>
                     
                    </div>
                  </div>
                 
                </div>
               
                <div class="form-group p-0" id="form-inbond-edit">
                  
                </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    

@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/emn178/chartjs-plugin-labels/src/chartjs-plugin-labels.js"></script>
<script type="text/javascript">
var rows_selected = [];
let NoForm = 0;
   $(function() {
    var dataChart = {!!json_encode($populer)!!}
    var ctx = document.getElementById("myChart").getContext('2d');

    var labelsPopuler = dataChart.data_populer.map(function(e) {
      return e.nama;
    });

    var dataPopuer = dataChart.data_populer.map(function(e) {
      return e.total;
    });

    var dataGambar = dataChart.data_populer.map(function(e) {
      const data = {
                      src: e.link,
                      width: 30,
                      height: 30,
                    }
      return data;
    });

    // console.log("gambar", dataGambar);



    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: labelsPopuler,
          datasets: [{
            label: 'Barang keluar',
            data: dataPopuer,
            borderWidth: 2,
            backgroundColor: 'rgba(63,82,227,.8)',
            borderWidth: 0,
            borderColor: 'transparent',
            pointBorderWidth: 0,
            pointRadius: 3.5,
            pointBackgroundColor: 'transparent',
            pointHoverBackgroundColor: 'rgba(63,82,227,.8)',
          },
         ]
        },
        options: {
          plugins: {
            labels: {
              render: 'image',
              textMargin: 10,
              images: dataGambar
            }
          },
          legend: {
            display: false
          },
          scales: {
            yAxes: [{
              gridLines: {
                // display: false,
                drawBorder: false,
                color: '#f2f2f2',
              },
              ticks: {
                beginAtZero: true,
                // stepSize: 100,
                callback: function(value, index, values) {
                  return  value;
                }
              }
            }],
            xAxes: [{
              gridLines: {
                display: false,
                tickMarkLength: 15,
              }
            }]
          },
        }
      });
  

    dataTabel();
    $('#kategoriPost').submit(function(e){
        e.preventDefault();
        // var form = $(this).serialize();
        var form = new FormData(this);

        console.log(form);
        $.ajax({
            type: "POST",
            url: "/admin/dashboard/manage-produk/store",
            data: form,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data){
                console.log(data);
                if (data.status==false) {
                      $("#info").html(`<div class="alert alert_sm alert-fade alert_danger" style="animation-delay: .1s" data-fade-time="3">
                                          <div class="alert--icon">
                                            <i class="fas fa-bell"></i>
                                          </div>
                                          <div class="alert--content">
                                            `+data.data+`
                                          </div>
                                          <div class="alert--close">
                                            <i class="far fa-times-circle"></i>
                                          </div>
                                      </div>`);
                      $('.alert--close').click(function () {
                            $('.alert_sm').addClass('alert_none')
                        });           
                }else{
                  $('#table-1').DataTable().destroy();
                  dataTabel();
                  $('#exampleModal').modal('hide');
                  $(".info-alert").html(`<div class="alert alert_sm alert-fade alert_success" style="animation-delay: .1s" data-fade-time="3">
                                          <div class="alert--icon">
                                            <i class="fas fa-bell"></i>
                                          </div>
                                          <div class="alert--content">
                                            `+data.data+`
                                          </div>
                                          <div class="alert--close">
                                            <i class="far fa-times-circle"></i>
                                          </div>
                                      </div>`);
                      $('.alert--close').click(function () {
                            $('.alert_sm').addClass('alert_none')
                        });  
                }
            }
        });
      });



      $('#table-1 tbody').on('click', 'td.btn-detail', function () {

          var tr = $(this).closest('tr');
          var row = $('#table-1').DataTable().row( tr );

          console.log(row.data());
          dataTabelIStok(row.data())
          $('#info-edit').html(row.data().nama_produk)
          $('#link_gambar').attr('src', row.data().link_gambar)
          
      });


      
    });


    function dataTabel() {
      var table = $('#table-1').DataTable({
                  "processing": true,
                  "bDestroy": true,
                  // "bFilter": false, 
                  "bInfo": true,
                  "order":[0,"asc"],
                  "bSortable": false,
                  "bPaginate": true,
                  "bLengthChange": false,
                  // "bFilter": false,
                  "retrieve": true, 
                  ajax : {
                      url : "/admin/dashboard/manage-stok-barang/datatabel",
                      type : "GET",
                      method : "GET",
                      dataSrc : "",
                  },
                  "columnDefs" : [
                            {
                                "targets": 0,
                                // "visible": false,
                                "className": "text-center",
                                "width": "5%" ,
                                render:function(data,type,row,meta){

                                    return  meta.row + meta.settings._iDisplayStart+1

                                }
                            },
                            {
                                "targets": 1,
                                // "visible": false,
                                // "width": "5%" ,
                                render:function(data,type,row,meta){
                                  
                                    return  row.nama_produk
                                }
                            },
                            {
                                "targets": 2,
                                // "visible": false,
                                // "width": "5%" ,
                                render:function(data,type,row,meta){
                                  
                                    return  row.stok_barang
                                }
                            },
                            {
                                "targets": 3,
                                // "visible": false,
                                // "width": "5%" ,
                                render:function(data,type,row,meta){
                                  
                                    return  row.out_bond
                                }
                            },
                            {
                                "targets": 4,
                                // "visible": false,
                                // "width": "5%" ,
                                render:function(data,type,row,meta){
                                  
                                    return  row.satua_produk
                                }
                            },
                            {
                                "targets": 5,
                                // "visible": false,
                                "className": "btn-detail",
                                "width": "10%" ,
                                render:function(data,type,row,meta){
                                  
                                    return  `<button class="btn btn-icon btn-sm btn-info btn-detail"  data-toggle="modal" data-target="#exampleModalEdit"><li class="ion ion-eye" data-pack="default" data-tags=""></li></button>`
                                }
                            },
                          ],
        }                
      );

    }

    function dataTabelIStok(params) {
      // console.log(params);
      $('#table-inbond').DataTable().destroy();
      $('#table-outbond').DataTable().destroy();
      $('#table-inbond').DataTable({
                  "processing": true,
                  "bDestroy": true,
                  // "bFilter": false, 
                  "bInfo": true,
                  "order":[0,"asc"],
                  "bSortable": false,
                  "bPaginate": true,
                  "bLengthChange": false,
                  // "bFilter": false,
                  "retrieve": true, 
                  ajax : {
                      url : "/admin/dashboard/manage-stok-barang/detail-transaksi?kode=" + params.kode_produk,
                      type : "GET",
                      method : "GET",
                      dataSrc: "data_in"
                  },
                  "columns": [
                      { "data": "no" },
                      { "data": "kode_transaksi" },
                      { "data": "created_at" },
                      { "data": "jumlah" }
                  ]
        }                
      );

      $('#table-outbond').DataTable({
                  "processing": true,
                  "bDestroy": true,
                  // "bFilter": false, 
                  "bInfo": true,
                  "order":[0,"asc"],
                  "bSortable": false,
                  "bPaginate": true,
                  "bLengthChange": false,
                  // "bFilter": false,
                  "retrieve": true, 
                  ajax : {
                      url : "/admin/dashboard/manage-stok-barang/detail-transaksi?kode=" + params.kode_produk,
                      type : "GET",
                      method : "GET",
                      dataSrc: "data_out"
                  },
                  "columns": [
                      { "data": "no" },
                      { "data": "kode_transaksi" },
                      { "data": "created_at" },
                      { "data": "jumlah" }
                  ]
        }                
      );

      // $.ajax({
      //       type: "GET",
      //       url: "/admin/dashboard/manage-stok-barang/detail-transaksi?kode=" + params.kode_produk,
      //       success: function(data){
      //           console.log(data);



      //           // let htmlDataIn = ''
      //           // let htmlDataOut = ''

      //           // for (let index = 0; index < data_in.length; index++) {
      //           //   htmlDataIn += 
      //           // }

      //           // for (let index = 0; index < data_out.length; index++) {
                  
      //           // }

      //           // $('#form-inbond-edit').html(htmlData)


      //       }
      //   });
      
    }
    
</script>
@endsection