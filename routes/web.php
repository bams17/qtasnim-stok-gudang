<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/mantap', function () {
   
// });


Route::prefix('/admin')->group(function () {

    
    Route::get('/', 'Admin\Auth\loginController@index');
    
    Route::prefix('/auth')->group(function () {
        Route::get('/admin-logout-auth', 'Admin\Auth\loginController@logout');
        Route::post('/admin-login-auth', 'Admin\Auth\loginController@login');

    });

    Route::prefix('/dashboard')->middleware('auth.admin')->group(function () {

        Route::get('/', 'Admin\adminController@index');

        Route::prefix('/manage-admin')->group(function () {
            Route::get('/', 'Admin\Auth\registrasiController@index');
            Route::get('/create', 'Admin\Auth\registrasiController@create');
            Route::get('/edit', 'Admin\Auth\registrasiController@edit');
            Route::post('/store', 'Admin\Auth\registrasiController@store');
            Route::post('/update/{id}', 'Admin\Auth\registrasiController@update');
            Route::post('/delete', 'Admin\Auth\registrasiController@delete');
            Route::get('/show', 'Admin\Auth\registrasiController@show');

        });

        Route::prefix('/manage-produk')->group(function () {
            Route::get('/', 'Admin\Produk\produkController@index');
            Route::post('/store', 'Admin\Produk\produkController@store');
            Route::post('/update', 'Admin\Produk\produkController@update');
            Route::post('/delete', 'Admin\Produk\produkController@destroy');
            Route::get('/datatabel', 'Admin\Produk\produkController@dataTabel');
        });

        Route::prefix('/manage-harga')->group(function () {
            Route::get('/', 'Admin\Harga\hargaController@index');
            Route::post('/store', 'Admin\Harga\hargaController@store');
            Route::post('/update', 'Admin\Harga\hargaController@update');
            Route::get('/datatabel', 'Admin\Harga\hargaController@dataTabel');
        });

        Route::prefix('/manage-stok-barang')->group(function () {
            Route::get('/', 'Admin\Stok\stokController@index');
            Route::get('/datatabel', 'Admin\Stok\stokController@dataTabelStok');
            Route::get('/detail-transaksi', 'Admin\Stok\stokController@dataTabelTransaksi');

            // Inbound
            Route::get('/inbond', 'Admin\Stok\stokController@indexInBond');
            Route::post('/store-inbond', 'Admin\Stok\stokController@storeInbond');
            Route::post('/update-inbond', 'Admin\Stok\stokController@updateInBond');
            Route::get('/datatabel-inbond', 'Admin\Stok\stokController@dataTabelInBond');
            Route::get('/detail-inbond', 'Admin\Stok\stokController@detailInBond');

            // Outbound BOND
            Route::get('/outbond', 'Admin\Stok\stokController@indexOutBond');
            Route::post('/store-outbond', 'Admin\Stok\stokController@storeOutBond');
            Route::post('/update-outbond', 'Admin\Stok\stokController@updateOutBond');
            Route::get('/datatabel-outbond', 'Admin\Stok\stokController@dataTabelOutBond');
            Route::get('/detail-outbond', 'Admin\Stok\stokController@detailIOutBond');

        });


    });

});


Route::prefix('/')->group(function () {
    Route::get('/', 'Admin\Auth\loginController@index');
    // Route::get('/', 'User\Home\homeController@index');
    // Route::get('/detail/{slug}', 'User\Berita\beritaController@show');
    // Route::get('/detail-berita', 'User\Berita\beritaController@show');

});









