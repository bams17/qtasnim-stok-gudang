<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
| API cek lagi bos sayang sekali ggmm
*/


Route::prefix('auth')->group(function(){
    Route::post('post/login','API\Auth\AuthController@login');
    Route::get('get/user','API\Auth\AuthController@userProfile');
    Route::post('post/registrasi','API\Auth\AuthController@registrasi');
    Route::post('post/update-token', 'API\Auth\AuthController@updateToken');

    Route::post('post/lupa-password', 'API\Auth\AuthController@lupaPassword');
    Route::post('post/verifikasi-kode', 'API\Auth\AuthController@cekverfikasicode');
    Route::post('post/rubah-password', 'API\Auth\AuthController@rubahpassword');


    Route::get('get/provinsi', 'API\Auth\AuthController@provinsi');
    Route::get('get/kota', 'API\Auth\AuthController@kota');
    Route::get('get/kecamatan', 'API\Auth\AuthController@kecamatan');
    Route::get('get/desa', 'API\Auth\AuthController@desa');
    Route::get('get/kodepos', 'API\Auth\AuthController@kodepos');

});

Route::prefix('produk')->group(function(){
    Route::get('get/list-harga','API\Produk\produkController@index');
});

Route::prefix('lembaga')->group(function(){
    Route::get('get/list','API\Tim\timController@index');
});

Route::prefix('notifikasi')->group(function(){
    Route::get('get/list-notifikasi','API\Tanam\tanamController@notifikasilist');
    Route::post('post/update-notifikasi','API\Tanam\tanamController@updateNotifikasi');

    Route::get('get/total-jual-tanam','API\Tanam\tanamController@totalDataTanam');
    Route::get('get/total-pengajuan','API\Mitra\mitraController@totalPengajuan');

});

Route::prefix('tanam')->group(function(){
    Route::post('post/tanam','API\Tanam\tanamController@store');
    Route::get('get/list-tanam','API\Tanam\tanamController@index');
    Route::get('get/list-tanam-select','API\Tanam\tanamController@listdatatanam');
    Route::get('get/detail-tanam','API\Tanam\tanamController@show');
    Route::post('post/hapus','API\Tanam\tanamController@destroy');

    Route::post('post/jual-tanam','API\Tanam\tanamController@jualtanam');
    Route::get('get/list-status-jual','API\Tanam\tanamController@listjualtanam');
    Route::get('get/detail-jual','API\Tanam\tanamController@detailjualtanam');
});

Route::prefix('news')->group(function(){
    Route::get('get/list','API\News\NewsController@kontens');
    Route::get('get/detail','API\News\NewsController@detailkonten');
});

Route::prefix('wilayah')->group(function(){
    Route::get('get/list/kota','API\Wilayah\wilayahController@kota');
    Route::get('get/list/kecamatan','API\Wilayah\wilayahController@kecamatan');
    Route::get('get/list/desa','API\Wilayah\wilayahController@desa');
    Route::get('get/list/lokasi','API\Wilayah\wilayahController@getDataLokasi');
});


Route::prefix('mitra-sourcing')->group(function(){
    Route::get('get/list/pengajuan','API\Mitra\mitraController@listpengajuan');
    Route::get('get/detail-jual','API\Tanam\tanamController@detailjualtanam');


    Route::post('post/proses-status-tolak','API\Tanam\tanamController@updatestatusTolak');
    Route::post('post/proses-status-diterima','API\Tanam\tanamController@updatestatusDiterima');
    Route::post('post/proses-status-survei','API\Tanam\tanamController@updatestatusSurvei');
    Route::post('post/proses-status-pembayaran','API\Tanam\tanamController@updatestatusBayar');
    Route::post('post/proses-status-pelunasan','API\Tanam\tanamController@updatestatusPelunasan');
    Route::post('post/proses-status-selesai','API\Tanam\tanamController@updatestatusSelesai');

});

